//
//  Session.swift
//  DIOS Transportation
//
//  Created by mac on 19/03/19.
//  Copyright © 2019 mac. All rights reserved.
//

import Foundation


fileprivate enum SessionKey: String {
    
    case loggedUserID = "loggedUserID"
    case email = "email"
    case name = "Name"
    case number = "Number"
    case city = "City"
    case state = "State"
    case zip = "Zip"
    case brandInspecter = "brandInspecter"
    case licenceNo = "LicenceNo"
    case headquarterCity = "headquarterCity"
    case ProfilePic = "ProfilePic"
    case SignaturePic = "SignaturePic"
    case SessionKeyAPI = "SessionKey"
    case strWeeklyStartDate = "strWeeklyStartDate"
    case strWeeklyEndDate = "strWeeklyEndDate"
    case AddressLine1 = "AddressLine1"
    case AddressLine2 = "AddressLine2"
    case strSelectedAddTaskDate = "strSelectedAddTaskDate"
    case strExpenseIndex = "strExpenseIndex"
    case HeadCount1 = "HeadCount1"
    case PositionId = "PositionId"
    case UserTypes = "UserTypes"
    
    
    func set(_ value: String) {
        UserDefaults.standard.set(value, forKey: rawValue)
    }
    
    func set(_ value: Bool) {
        UserDefaults.standard.set(value, forKey: rawValue)
    }
    
    func set(_ value: Int) {
        UserDefaults.standard.set(value, forKey: rawValue)
    }
    
    
    func string() -> String? {
        return UserDefaults.standard.string(forKey: rawValue)
    }
    
    func bool() -> Bool {
        return UserDefaults.standard.bool(forKey: rawValue)
    }
    
    func int() -> Int {
        return UserDefaults.standard.integer(forKey: rawValue)
    }
    
}

class Session: NSObject {
    
    //This prevents others from using the default '()' initializer for this class.
    private override init(){}
    
    static var email: String {
        
        get{
            let email = SessionKey.email
            return email.string() ?? ""
        }
        
        set(value){
            let email = SessionKey.email
            email.set(value)
        }
        
    }

    static var loggedUserID: String {
        
        get{
            let loggedUserID = SessionKey.loggedUserID
            return loggedUserID.string() ?? ""
        }
        
        set(value){
            let loggedUserID = SessionKey.loggedUserID
            loggedUserID.set(value)
        }
        
    }

    static var name: String {
        
        get{
            let name = SessionKey.name
            return name.string() ?? ""
        }
        
        set(value){
            let name = SessionKey.name
            name.set(value)
        }
        
    }
    
    static var number: String {
        
        get{
            let number = SessionKey.number
            return number.string() ?? ""
        }
        
        set(value){
            let number = SessionKey.number
            number.set(value)
        }
        
    }
    
    static var city: String {
        
        get{
            let city = SessionKey.city
            return city.string() ?? ""
        }
        
        set(value){
            let city = SessionKey.city
            city.set(value)
        }
        
    }
    
    static var state: String {
        
        get{
            let state = SessionKey.state
            return state.string() ?? ""
        }
        
        set(value){
            let state = SessionKey.state
            state.set(value)
        }
        
    }
    
    static var zip: String {
        
        get{
            let zip = SessionKey.zip
            return zip.string() ?? ""
        }
        
        set(value){
            let zip = SessionKey.zip
            zip.set(value)
        }
        
    }
    
    static var brandInspecter: String {
        
        get{
            let brandInspecter = SessionKey.brandInspecter
            return brandInspecter.string() ?? ""
        }
        
        set(value){
            let brandInspecter = SessionKey.brandInspecter
            brandInspecter.set(value)
        }
        
    }
    
    static var licenceNo: String {
        
        get{
            let licenceNo = SessionKey.licenceNo
            return licenceNo.string() ?? ""
        }
        
        set(value){
            let licenceNo = SessionKey.licenceNo
            licenceNo.set(value)
        }
    }
    
    static var headquarterCity: String {
        
        get{
            let headquarterCity = SessionKey.headquarterCity
            return headquarterCity.string() ?? ""
        }
        
        set(value){
            let headquarterCity = SessionKey.headquarterCity
            headquarterCity.set(value)
        }
    }
    
    static var ProfilePic: String {
        
        get{
            let ProfilePic = SessionKey.ProfilePic
            return ProfilePic.string() ?? ""
        }
        
        set(value){
            let ProfilePic = SessionKey.ProfilePic
            ProfilePic.set(value)
        }
    }
    
    static var SignaturePic: String {
         
         get{
             let SignaturePic = SessionKey.SignaturePic
             return SignaturePic.string() ?? ""
         }
         
         set(value){
             let SignaturePic = SessionKey.SignaturePic
             SignaturePic.set(value)
         }
     }
    
    static var SessionKeyAPI: String {
        
        get{
            let SessionKeyAPI = SessionKey.SessionKeyAPI
            return SessionKeyAPI.string() ?? ""
        }
        
        set(value){
            let SessionKeyAPI = SessionKey.SessionKeyAPI
            SessionKeyAPI.set(value)
        }
    }

    static var strWeeklyStartDate: String {
        
        get{
            let strWeeklyStartDate = SessionKey.strWeeklyStartDate
            return strWeeklyStartDate.string() ?? ""
        }
        
        set(value){
            let strWeeklyStartDate = SessionKey.strWeeklyStartDate
            strWeeklyStartDate.set(value)
        }
    }
    
    static var strWeeklyEndDate: String {
        
        get{
            let strWeeklyEndDate = SessionKey.strWeeklyEndDate
            return strWeeklyEndDate.string() ?? ""
        }
        
        set(value){
            let strWeeklyEndDate = SessionKey.strWeeklyEndDate
            strWeeklyEndDate.set(value)
        }
    }

    static var AddressLine1: String {
        
        get{
            let AddressLine1 = SessionKey.AddressLine1
            return AddressLine1.string() ?? ""
        }
        
        set(value){
            let AddressLine1 = SessionKey.AddressLine1
            AddressLine1.set(value)
        }
    }
    
    static var AddressLine2: String {
        
        get{
            let AddressLine2 = SessionKey.AddressLine2
            return AddressLine2.string() ?? ""
        }
        
        set(value){
            let AddressLine2 = SessionKey.AddressLine2
            AddressLine2.set(value)
        }
    }
    
    static var strSelectedAddTaskDate: String {
        
        get{
            let strSelectedAddTaskDate = SessionKey.strSelectedAddTaskDate
            return strSelectedAddTaskDate.string() ?? ""
        }
        
        set(value){
            let strSelectedAddTaskDate = SessionKey.strSelectedAddTaskDate
            strSelectedAddTaskDate.set(value)
        }
    }
    
    static var strExpenseIndex: String {
        
        get{
            let strExpenseIndex = SessionKey.strExpenseIndex
            return strExpenseIndex.string() ?? ""
        }
        
        set(value){
            let strExpenseIndex = SessionKey.strExpenseIndex
            strExpenseIndex.set(value)
        }
    }
    
    static var HeadCount1: String {
        
        get{
            let HeadCount1 = SessionKey.HeadCount1
            return HeadCount1.string() ?? ""
        }
        
        set(value){
            let HeadCount1 = SessionKey.HeadCount1
            HeadCount1.set(value)
        }
    }
    
    static var PositionId: String {
        
        get{
            let PositionId = SessionKey.PositionId
            return PositionId.string() ?? ""
        }
        
        set(value){
            let PositionId = SessionKey.PositionId
            PositionId.set(value)
        }
    }
    
    static var UserTypes: String {
        
        get{
            let UserTypes = SessionKey.UserTypes
            return UserTypes.string() ?? ""
        }
        
        set(value){
            let UserTypes = SessionKey.UserTypes
            UserTypes.set(value)
        }
    }
}
