//
//  APIManager.swift
//  DIOS Transportation
//
//  Created by mac on 12/03/19.
//  Copyright © 2019 mac. All rights reserved.
//

import Foundation
import UIKit
import SystemConfiguration
import Alamofire
import JTProgressHUD

typealias JSONResponse = [String : Any]
typealias APIParameters = [String : Any]

extension Constant{
    
    struct API {
    
       // static let serverUrl = "http://192.168.1.113/api/Values/"
       // static let serverUrl = "http://192.168.1.67/api/Values/"
        static let serverUrl = "http://180.211.98.106:9091/api/Values/"
       // static let serverUrl = "http://180.211.98.106:9092/api/Values/"
        static let imageUrl = ""
    
        static let headers: HTTPHeaders = [
            "Content-Type": "application/x-www-form-urlencoded" //"X-Auth" : "633uq4t0qdmdlfdgkerlope_uilna4334"
        ]
        
        enum Name: String {
            
            case login                  = "Login"
            case getTimeCode            = "GetTimeCode"
            case getTaskData            = "GetTaskData"
            case addTask                = "AddTask"
            case addStandardExpense     = "AddStandardExpense"
            case addTravelExpense       = "AddTravelExpense"
            case getStaffDate           = "GetStaffName"
            case AddTimeOfRequest       = "AddTimeOfRequest"
            case GetDailyDetailReport   = "GetDailyDetailReport"
            case LogOut                 = "PostLogOff"
            case UserCodeDetailReport   = "UserCodeDetailReport"
            case UserLeaveDetailList    = "UserLeaveDetailList"
            case PostEditUser           = "PostEditUser"
            case PostTaskDelete         = "PostTaskDelete"
            case PostEditUser1          = "PostEditUser1"
            case PostMileageRate        = "PostMileageRate"
            case UpdateTaskWeekDetail   = "UpdateTaskWeekDetail"
            case APIStandardExpenseList = "APIStandardExpenseList"
            case APITravelExpenseList   = "APITravelExpenseList"
            case PostTimeOfRequestDelete  = "PostTimeOfRequestDelete"
            
            var url: String{
                return "\(serverUrl)\(rawValue)"
            }
        }
    }
}

struct Constant {
    
}


extension UIViewController {
    
    func removeNSNull(from dict: [String: Any]) -> [String: Any] {
        var mutableDict = dict
        let keysWithEmptString = dict.filter { $0.1 is NSNull }.map { $0.0 }
        for key in keysWithEmptString {
            mutableDict[key] = ""
        }
        return mutableDict
    }
}

struct CustomPostEncoding: ParameterEncoding {
    func encode(_ urlRequest: URLRequestConvertible, with parameters: Parameters?) throws -> URLRequest {
        var request = try URLEncoding().encode(urlRequest, with: parameters)
        let httpBody = NSString(data: request.httpBody!, encoding: String.Encoding.utf8.rawValue)!
        request.httpBody = httpBody.replacingOccurrences(of: "%5B%5D=", with: "=").data(using: .utf8)
        return request
    }
}


class APIManager: NSObject{
    
    //    static let shared = APIManager()
    private override init() { }
    
    
    class func removeNSNull(from dict: [String: Any]) -> [String: Any] {
        var mutableDict = dict
        let keysWithEmptString = dict.filter { $0.1 is NSNull }.map { $0.0 }
        for key in keysWithEmptString {
            mutableDict[key] = ""
        }
        return mutableDict
    }
    
    class func postRequest(apiName: Constant.API.Name,parameter: APIParameters, success: @escaping(_ response: JSONResponse)-> Void, errorResponse: @escaping(_ response: String)-> Void ) {
        JTProgressHUD.show(with: .blurDark)
        if Reachable.isConnectedToNetwork(){
            print("=========URL======\n" + apiName.url)
            print("=========Parameter======\n \(parameter)")
            
            
            Alamofire.request(apiName.url, method: .post, parameters: parameter, encoding: URLEncoding.default, headers: Constant.API.headers).responseJSON { (dataResponse) in
                JTProgressHUD.hide()
                print(dataResponse.result.value)
                if let dataDict = dataResponse.result.value as? JSONResponse{
                    success(dataDict)
                    
                } else {
                    errorResponse("Something went wrong")
                }
             }
        } else {
            UIApplication.topViewController()?.alert(title: "Error", message: "Can not connect to internet")
        }
    }
    
    class func deleteRequest(apiName: Constant.API.Name,parameter: APIParameters, success: @escaping(_ response: JSONResponse)-> Void, errorResponse: @escaping(_ response: String)-> Void ) {
        JTProgressHUD.show(with: .blurDark)
        if Reachable.isConnectedToNetwork(){
            print("=========URL======\n" + apiName.url)
            print("=========Parameter======\n \(parameter)")
            
            
            Alamofire.request(apiName.url, method: .delete, parameters: parameter, encoding: URLEncoding.default, headers: Constant.API.headers).responseJSON { (dataResponse) in
                JTProgressHUD.hide()
                print(dataResponse.result.value)
                if let dataDict = dataResponse.result.value as? JSONResponse{
                    success(dataDict)
                    
                } else {
                    errorResponse("Something went wrong")
                }
            }
        } else {
            UIApplication.topViewController()?.alert(title: "Error", message: "Can not connect to internet")
        }
    }
    
    class func getRequest(apiName: Constant.API.Name,parameter: APIParameters, success: @escaping(_ response: JSONResponse)-> Void ,errorResponse: @escaping(_ response: String)-> Void ) {
        JTProgressHUD.show(with: .blurDark)
        if Reachable.isConnectedToNetwork(){
            
            
            Alamofire.request(apiName.url, method: .get, parameters: parameter, encoding: URLEncoding.default, headers: nil).responseJSON { (dataResponse) in
                print("=========URL======\n" + apiName.url)
                print("=========Parameter======\n \(parameter))")
                JTProgressHUD.hide()
             //   UIApplication.topViewController()?.hideActivity()
                
                if let dataDict = dataResponse.result.value as? JSONResponse{
                    let dict = removeNSNull(from: dataDict)
                    success(dict)
                } else {
                    errorResponse("Something went wrong")
                }
            }
            
        } else {
            
          //  UIApplication.topViewController()?.hideActivity()
            UIApplication.topViewController()?.alert(title: "Error", message: "Can not connect to internet")
        }
    }
}
