//
//  Redirection.swift
//  MotherShip
//
//  Created by Admin on 04/03/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import Foundation
import UIKit

protocol RedirectionProtocol {
    
    static func getViewController() -> UIViewController
    
}

// MARK: ==================== Main Storyboard ===========================
protocol MainProtocol: RedirectionProtocol {
    
    static var storyboard: UIStoryboard { get }
    
    static var storyboardIdentifier: String { get }
    
}


extension MainProtocol where Self: UIViewController {
    
    
    static var storyboard: UIStoryboard {
        return UIStoryboard(name: "Main", bundle: nil)
    }
    
    
    static var storyboardIdentifier: String {
        return String(describing: self)
    }
    
    
    private static func fromStoryboard() -> Self {
        
        let viewController = storyboard.instantiateViewController(withIdentifier: storyboardIdentifier) as! Self
        return viewController
    }
    
    static func getViewController() -> UIViewController {
        return fromStoryboard()
    }
    
}

// MARK: ==================== Home Storyboard ===========================

protocol HomeProtocol: RedirectionProtocol {
    
    static var storyboard: UIStoryboard { get }
    
    static var storyboardIdentifier: String { get }
    
}


extension HomeProtocol where Self: UIViewController {
    
    
    static var storyboard: UIStoryboard {
        return UIStoryboard(name: "Home", bundle: nil)
    }
    
    
    static var storyboardIdentifier: String {
        return String(describing: self)
    }
    
    
    private static func fromStoryboard() -> Self {
        
        let viewController = storyboard.instantiateViewController(withIdentifier: storyboardIdentifier) as! Self
        return viewController
    }
    
    static func getViewController() -> UIViewController {
        return fromStoryboard()
    }
    
}


protocol CreateStationProtocol: RedirectionProtocol {
    
    static var storyboard: UIStoryboard { get }
    
    static var storyboardIdentifier: String { get }
    
}

extension CreateStationProtocol where Self: UIViewController {
    
    
    static var storyboard: UIStoryboard {
        return UIStoryboard(name: "CreateStation", bundle: nil)
    }
    
    
    static var storyboardIdentifier: String {
        return String(describing: self)
    }
    
    
    private static func fromStoryboard() -> Self {
        
        let viewController = storyboard.instantiateViewController(withIdentifier: storyboardIdentifier) as! Self
        return viewController
    }
    
    static func getViewController() -> UIViewController {
        return fromStoryboard()
    }
    
}
