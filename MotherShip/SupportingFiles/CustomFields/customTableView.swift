//
//  customTableView.swift
//  MotherShip
//
//  Created by Admin on 19/03/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit
@IBDesignable
class customTableView : UITableView {
    
    
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadius
            layer.masksToBounds = true
        }
    }
}
