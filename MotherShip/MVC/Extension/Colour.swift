//
//  Colour.swift
//  MotherShip
//
//  Created by Admin on 02/03/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import Foundation

import UIKit
//39
//81
//148
extension UIColor {
    
    public class var appBlue: UIColor {
        return UIColor(red: 21.0/255.0, green: 50.0/255.0, blue: 114.0/255.0, alpha: 1.0)
    }
    
    public class var tabBarTextColor: UIColor {
        return UIColor(red: 46.0/255.0, green: 26.0/255.0, blue: 100.0/255.0, alpha: 1.0)
    }
    
    public class var tabBarBackground: UIColor {
        return UIColor(red: 10.0/255.0, green: 31.0/255.0, blue: 73.0/255.0, alpha: 1.0)
    }
    
    public class var lightGrayText: UIColor {
        return UIColor(red: 128.0/255.0, green: 128.0/255.0, blue: 129.0/255.0, alpha: 1.0)
    }
    
    public class var buttonBackground: UIColor {
        return UIColor(red: 141.0/255.0, green: 173.0/255.0, blue: 247.0/255.0, alpha: 1.0)
    }
    
    public class var placeHolderColor: UIColor {
        return UIColor(red: 87.0/255.0, green: 102.0/255.0, blue: 139.0/255.0, alpha: 1.0)
    }
    
    public class var sagmantegControlBG: UIColor {
        return UIColor(red: 16.0/255.0, green: 36.0/255.0, blue: 83.0/255.0, alpha: 1.0)
    }
    
}

class PlaceHolderTextField: UITextField {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        guard let holder = placeholder, !holder.isEmpty else { return }
        attributedPlaceholder = NSAttributedString(string: holder, attributes: [.foregroundColor: UIColor.placeHolderColor])
    }
}
