//
//  Font.swift
//  Destination
//
//  Created by mac on 14/03/19.
//  Copyright © 2019 mac. All rights reserved.
//

import Foundation
import UIKit


extension UIFont{


    class func custom(size: CGFloat) -> UIFont {
        return UIFont(name: "Montserrat-Regular", size: size)!
    }

    class func customBold(size: CGFloat) -> UIFont {
        return UIFont(name: "Montserrat-Bold", size: size)!
    }

    class func bold() -> UIFont {
        return UIFont(name: "Montserrat-Bold", size: 14)!
    }

    class func regular() -> UIFont {
        return UIFont(name: "Montserrat-Regular", size: 14)!
    }
    
    class var extraLight : UIFont! {
        return UIFont(name: "Montserrat-Light", size: 14)!
    }
    
    class func customExtraLight(size: CGFloat) -> UIFont {
        return UIFont(name: "Montserrat-Light", size: size)!
    }
}
