//
//  FormTextField.swift
//  MotherShip
//
//  Created by Admin on 02/03/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import Foundation
import UIKit


class FloatingLabelInput: UITextField {
    var floatingLabel: UILabel!// = UILabel(frame: CGRect.zero)
    var floatingLabelHeight: CGFloat = 14
    var button = UIButton(type: .custom)
    var imageView = UIImageView(frame: CGRect.zero)
    
    @IBInspectable
    var _placeholder: String?
    
    @IBInspectable
    var floatingLabelColor: UIColor = UIColor.lightGrayText {
        didSet {
            self.floatingLabel.textColor = floatingLabelColor
            self.setNeedsDisplay()
        }
    }
    
    @IBInspectable
    var activeBorderColor: UIColor = UIColor.blue
    
    @IBInspectable
    var floatingLabelBackground: UIColor = UIColor.clear.withAlphaComponent(1) {
        didSet {
            self.floatingLabel.backgroundColor = self.floatingLabelBackground
            self.setNeedsDisplay()
        }
    }
    
    @IBInspectable
    var floatingLabelFont: UIFont = UIFont.regular() {
        didSet {
            self.floatingLabel.font = self.floatingLabelFont
            self.font = self.floatingLabelFont
            self.setNeedsDisplay()
        }
    }
    
    @IBInspectable
    var _backgroundColor: UIColor = UIColor.clear {
        didSet {
            self.layer.backgroundColor = self._backgroundColor.cgColor
        }
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self._placeholder = (self._placeholder != nil) ? self._placeholder : placeholder
        placeholder = self._placeholder // Make sure the placeholder is shown
        self.floatingLabel = UILabel(frame: CGRect.zero)
        self.addTarget(self, action: #selector(self.addFloatingLabel), for: .editingDidBegin)
        self.addTarget(self, action: #selector(self.removeFloatingLabel), for: .editingDidEnd)
    }
    
    // Add a floating label to the view on becoming first responder
    @objc func addFloatingLabel() {
        if self.text == "" {
            self.floatingLabel.textColor = floatingLabelColor
            self.floatingLabel.font = floatingLabelFont
            self.floatingLabel.text = self._placeholder
            self.floatingLabel.layer.backgroundColor = UIColor.clear.cgColor
            self.floatingLabel.translatesAutoresizingMaskIntoConstraints = false
            self.floatingLabel.clipsToBounds = true
            self.floatingLabel.frame = CGRect(x: 0, y: 0, width: floatingLabel.frame.width+4, height: floatingLabel.frame.height+2)
            self.floatingLabel.textAlignment = .center
            self.addSubview(self.floatingLabel)
            self.layer.borderColor = self.activeBorderColor.cgColor
            
            self.floatingLabel.bottomAnchor.constraint(equalTo: self.topAnchor, constant: -10).isActive = true // Place our label 10 pts above the text field
            self.placeholder = ""
        }
        // Floating label may be stuck behind text input. we bring it forward as it was the last item added to the view heirachy
        self.bringSubviewToFront(subviews.last!)
        self.setNeedsDisplay()
    }
    
    @objc func removeFloatingLabel() {
        if self.text == "" {
            UIView.animate(withDuration: 0.13) {
                self.subviews.forEach{ $0.removeFromSuperview() }
                self.setNeedsDisplay()
            }
            self.placeholder = self._placeholder
        }
        self.layer.borderColor = UIColor.black.cgColor
    }
    
    func addViewPasswordButton() {
        self.button.setImage(UIImage(named: "ic_reveal"), for: .normal)
        self.button.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        self.button.frame = CGRect(x: 0, y: 16, width: 22, height: 16)
        self.button.clipsToBounds = true
        self.rightView = self.button
        self.rightViewMode = .always
        self.button.addTarget(self, action: #selector(self.enablePasswordVisibilityToggle), for: .touchUpInside)
    }
    
    func addImage(image: UIImage){
        
        self.imageView.image = image
        self.imageView.frame = CGRect(x: 20, y: 0, width: 20, height: 20)
        self.imageView.translatesAutoresizingMaskIntoConstraints = true
        self.imageView.contentMode = .scaleAspectFit
        self.imageView.clipsToBounds = true
        
        DispatchQueue.main.async {
            self.leftView = self.imageView
            self.leftViewMode = .always
        }
        
    }
    
    @objc func enablePasswordVisibilityToggle() {
        isSecureTextEntry.toggle()
        if isSecureTextEntry {
            self.button.setImage(UIImage(named: "ic_show"), for: .normal)
        }else{
            self.button.setImage(UIImage(named: "ic_hide"), for: .normal)
        }
    }
}

struct UIViewBorderAttributeKey {
    static let marginTop = "marginTop"
    static let marginLeft = "marginLeft"
    static let marginRight = "marginRight"
    static let marginBottom = "marginBottom"
}


extension UIView {
    
    //FIXME:- ios 9.0 and later
//    public func addConstraintsWithFormat(_ format: String, views: UIView...) {
//
//        var viewsDictionary = [String: UIView]()
//        for (index, view) in views.enumerated() {
//            let key = "v\(index)"
//            viewsDictionary[key] = view
//            view.translatesAutoresizingMaskIntoConstraints = false
//        }
//
//        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: format, options: NSLayoutFormatOptions(), metrics: nil, views: viewsDictionary))
//    }
    
    public func fillSuperview() {
        translatesAutoresizingMaskIntoConstraints = false
        if let superview = superview {
            leftAnchor.constraint(equalTo: superview.leftAnchor).isActive = true
            rightAnchor.constraint(equalTo: superview.rightAnchor).isActive = true
            topAnchor.constraint(equalTo: superview.topAnchor).isActive = true
            bottomAnchor.constraint(equalTo: superview.bottomAnchor).isActive = true
        }
    }
    
    public func anchor(_ top: NSLayoutYAxisAnchor? = nil, left: NSLayoutXAxisAnchor? = nil, bottom: NSLayoutYAxisAnchor? = nil, right: NSLayoutXAxisAnchor? = nil, topConstant: CGFloat = 0, leftConstant: CGFloat = 0, bottomConstant: CGFloat = 0, rightConstant: CGFloat = 0, widthConstant: CGFloat = 0, heightConstant: CGFloat = 0) {
        translatesAutoresizingMaskIntoConstraints = false
        
        _ = anchorWithReturnAnchors(top, left: left, bottom: bottom, right: right, topConstant: topConstant, leftConstant: leftConstant, bottomConstant: bottomConstant, rightConstant: rightConstant, widthConstant: widthConstant, heightConstant: heightConstant)
    }
    
    public func anchorWithReturnAnchors(_ top: NSLayoutYAxisAnchor? = nil, left: NSLayoutXAxisAnchor? = nil, bottom: NSLayoutYAxisAnchor? = nil, right: NSLayoutXAxisAnchor? = nil, topConstant: CGFloat = 0, leftConstant: CGFloat = 0, bottomConstant: CGFloat = 0, rightConstant: CGFloat = 0, widthConstant: CGFloat = 0, heightConstant: CGFloat = 0) -> [NSLayoutConstraint] {
        translatesAutoresizingMaskIntoConstraints = false
        
        var anchors = [NSLayoutConstraint]()
        
        if let top = top {
            anchors.append(topAnchor.constraint(equalTo: top, constant: topConstant))
        }
        
        if let left = left {
            anchors.append(leftAnchor.constraint(equalTo: left, constant: leftConstant))
        }
        
        if let bottom = bottom {
            anchors.append(bottomAnchor.constraint(equalTo: bottom, constant: -bottomConstant))
        }
        
        if let right = right {
            anchors.append(rightAnchor.constraint(equalTo: right, constant: -rightConstant))
        }
        
        if widthConstant > 0 {
            anchors.append(widthAnchor.constraint(equalToConstant: widthConstant))
        }
        
        if heightConstant > 0 {
            anchors.append(heightAnchor.constraint(equalToConstant: heightConstant))
        }
        
        anchors.forEach({$0.isActive = true})
        
        return anchors
    }
    
    public func anchorCenterXToSuperview(constant: CGFloat = 0) {
        translatesAutoresizingMaskIntoConstraints = false
        if let anchor = superview?.centerXAnchor {
            centerXAnchor.constraint(equalTo: anchor, constant: constant).isActive = true
        }
    }
    
    public func anchorCenterYToSuperview(constant: CGFloat = 0) {
        translatesAutoresizingMaskIntoConstraints = false
        if let anchor = superview?.centerYAnchor {
            centerYAnchor.constraint(equalTo: anchor, constant: constant).isActive = true
        }
    }
    
    public func anchorCenterSuperview() {
        anchorCenterXToSuperview()
        anchorCenterYToSuperview()
    }
    
    
    
    
    //FIXME:- ios 8 and later
    
    fileprivate var topBorderTag: Int{ return 8001 }
    
    fileprivate var leftBorderTag: Int{ return 8002 }
    
    fileprivate var rightBorderTag: Int{ return 8003 }
    
    fileprivate var bottomBorderTag: Int{ return 8004 }
    
    private var defaultAttributes: [String : Any] {
        
        let attributes: [String : Any] = [
            UIViewBorderAttributeKey.marginTop : 0,
            UIViewBorderAttributeKey.marginLeft : 0,
            UIViewBorderAttributeKey.marginBottom : 0,
            UIViewBorderAttributeKey.marginRight : 0,
            ]
        
        return attributes
    }
    
    func drawTopBorder(width: Float, color: UIColor, attributes: [String : Any]){
        
        var metrics: [String : Any] = defaultAttributes
        attributes.forEach { metrics.updateValue($1, forKey: $0) }
        metrics["borderWidth"] = width
        
        let borderView = viewWithTag(topBorderTag) ?? UIView()
        borderView.translatesAutoresizingMaskIntoConstraints = false
        borderView.backgroundColor = color
        self.addSubview(borderView)
        
        let views: [String : Any] = [
            "borderView" : borderView
        ]
        
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-marginLeft-[borderView]-marginRight-|", options: [], metrics: metrics, views: views))
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[borderView(==borderWidth)]", options: [], metrics: metrics, views: views))
        
        layoutIfNeeded()
    }
    
    func drawLeftBorder(width: Float, color: UIColor, attributes: [String : Any]){
        
        var metrics: [String : Any] = defaultAttributes
        attributes.forEach { metrics.updateValue($1, forKey: $0) }
        metrics["borderWidth"] = width
        
        let borderView = viewWithTag(leftBorderTag) ?? UIView()
        borderView.translatesAutoresizingMaskIntoConstraints = false
        borderView.backgroundColor = color
        self.addSubview(borderView)
        
        let views: [String : Any] = [
            "borderView" : borderView
        ]
        
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "|[borderView(==borderWidth)]", options: [], metrics: metrics, views: views))
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-marginTop-[borderView]-marginBottom-|", options: [], metrics: metrics, views: views))
        
        layoutIfNeeded()
    }
    
    func drawRightBorder(width: Float, color: UIColor, attributes: [String : Any]){
        
        var metrics: [String : Any] = defaultAttributes
        attributes.forEach { metrics.updateValue($1, forKey: $0) }
        metrics["borderWidth"] = width
        
        let borderView = viewWithTag(rightBorderTag) ?? UIView()
        borderView.translatesAutoresizingMaskIntoConstraints = false
        borderView.backgroundColor = color
        self.addSubview(borderView)
        
        let views: [String : Any] = [
            "borderView" : borderView
        ]
        
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-(>=0)-[borderView(==borderWidth)]-marginRight-|", options: [], metrics: metrics, views: views))
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-marginTop-[borderView]-marginBottom-|", options: [], metrics: metrics, views: views))
        
        layoutIfNeeded()
    }
    
    func drawBottomBorder(width: Float, color: UIColor, attributes: [String : Any]){
        
        var metrics: [String : Any] = defaultAttributes
        attributes.forEach { metrics.updateValue($1, forKey: $0) }
        metrics["borderWidth"] = width
        
        let borderView = viewWithTag(bottomBorderTag) ?? UIView()
        borderView.tag = bottomBorderTag
        borderView.removeFromSuperview()
        borderView.translatesAutoresizingMaskIntoConstraints = false
        borderView.backgroundColor = color
        self.addSubview(borderView)
        
        let views: [String : Any] = [
            "borderView" : borderView
        ]
        
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-marginLeft-[borderView]-marginRight-|", options: [], metrics: metrics, views: views))
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-(>=0)-[borderView(==borderWidth)]-marginBottom-|", options: [], metrics: metrics, views: views))
        
        layoutIfNeeded()
    }
    
    func removeTopBorder(animation: Bool) {
        
        if let borderView = viewWithTag(topBorderTag) {
            if animation {
                UIView.animate(withDuration: 0.2, animations: {
                    borderView.removeFromSuperview()
                })
            }else{
                borderView.removeFromSuperview()
            }
            
        }
        
    }
    
    func removeLeftBorder(animation: Bool) {
        
        if let borderView = viewWithTag(leftBorderTag) {
            if animation {
                UIView.animate(withDuration: 0.2, animations: {
                    borderView.removeFromSuperview()
                })
            }else{
                borderView.removeFromSuperview()
            }
            
        }
        
    }
    
    func removeRightBorder(animation: Bool) {
        
        if let borderView = viewWithTag(rightBorderTag) {
            if animation {
                UIView.animate(withDuration: 0.2, animations: {
                    borderView.removeFromSuperview()
                })
            }else{
                borderView.removeFromSuperview()
            }
            
        }
        
    }
    
    func removeBottomBorder(animation: Bool) {
        
        if let borderView = viewWithTag(bottomBorderTag) {
            if animation {
                UIView.animate(withDuration: 0.2, animations: {
                    borderView.removeFromSuperview()
                })
            }else{
                borderView.removeFromSuperview()
            }
            
        }
        
    }
}
