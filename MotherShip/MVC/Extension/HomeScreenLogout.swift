//
//  HomeScreenLogout.swift
//  MotherShip
//
//  Created by Admin on 05/03/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController{
    
    func logOut()  {
        
        let controller = WelcomeVC.getViewController() as! WelcomeVC
        let navController = UINavigationController(rootViewController: controller)
        navController.setNavigationBarHidden(true, animated: true)
        let window = UIApplication.shared.keyWindowInConnectedScenes
        UIApplication.shared.switchRootViewController(window, rootViewController: navController, animation: .scale) {
            print("Account Module")
        }
    }
    
    func goToDashboard()  {
        let dashboard = TabBarVC.getViewController() as! TabBarVC
        let navController = UINavigationController(rootViewController: dashboard)
        navController.setNavigationBarHidden(true, animated: false)
        let window = UIApplication.shared.keyWindowInConnectedScenes
        UIApplication.shared.switchRootViewController(window, rootViewController: navController, animation: .scale) {
            print("Home Module")
        }
    }
    
    
    
}
extension UIBarButtonItem {

    var frame: CGRect? {
        guard let view = self.value(forKey: "view") as? UIView else {
            return nil
        }
        return view.frame
    }

}
