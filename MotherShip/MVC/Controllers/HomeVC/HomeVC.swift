//
//  HomeVC.swift
//  MotherShip
//
//  Created by Admin on 04/03/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit
import SideMenu
import MBCircularProgressBar


class HomeVC: UIViewController,HomeProtocol {
    
    @IBOutlet weak var tblMain: UITableView!
    
    @IBOutlet weak var viewProgressTotalProject: MBCircularProgressBarView!
    @IBOutlet weak var viewProgressInProgress: MBCircularProgressBarView!
    @IBOutlet weak var viewProgressComplete: MBCircularProgressBarView!
    
    @IBOutlet weak var viewTotalProject: UIView!
    @IBOutlet weak var viewInProgress: UIView!
    @IBOutlet weak var viewComplete: UIView!
    
    
    
    var arrMenu = NSArray()
    
    var menuItem = UIBarButtonItem()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        menuItem = UIBarButtonItem(image: UIImage(named: "imgIcon"), style: .done, target: self, action: #selector(openMenu))
        setMenu()
        
        let label = UILabel()
        label.font = .custom(size: 20)
        label.textColor = .white
        label.text = "Ground Countrol"
        label.textAlignment = .left
        
        let titleItem = UIBarButtonItem(customView: label)
        navigationItem.leftBarButtonItems = [menuItem,titleItem]
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        
        viewTotalProject.layer.cornerRadius = 10
        viewInProgress.layer.cornerRadius = 10
        viewTotalProject.layer.cornerRadius = 10
        
        
        arrMenu = [1,2,3,1,1,1,1,1,1,1,1]
        
    }
    override func viewDidAppear(_ animated: Bool) {
        
        setProgressView()
    }
    
    func setProgressView(){
        
        viewProgressTotalProject.maxValue = 100
        viewProgressTotalProject.progressLineWidth = 6.0
        viewProgressTotalProject.emptyLineWidth = 6.0
        
        viewProgressInProgress.maxValue = 100
        viewProgressInProgress.progressLineWidth = 6.0
        viewProgressInProgress.emptyLineWidth = 6.0
        
        viewProgressComplete.maxValue = 100
        viewProgressComplete.progressLineWidth = 6.0
        viewProgressComplete.emptyLineWidth = 6.0
        
        UIView.animate(withDuration: 2.0, animations: { () -> Void in
            self.viewProgressTotalProject.value = 100.0 - 60.0
            self.viewProgressInProgress.value = 100.0 - 20.0
            self.viewProgressComplete.value = 100.0 - 80.0
        })
        
    }
    
    
    // MARK: ==================== BUTTON CLICK ===========================
    @objc func openMenu(){
        present(SideMenuManager.default.leftMenuNavigationController!, animated: true, completion: nil)
    }
    
    
    // MARK: ==================== FUNCTION ===========================
    func setMenu(){
        let menuRightNavigationController = storyboard!.instantiateViewController(withIdentifier: "RightMenuNavigationController") as! SideMenuNavigationController
        SideMenuManager.default.leftMenuNavigationController = menuRightNavigationController
        menuRightNavigationController.settings.presentationStyle = .viewSlideOut
        
        SideMenuManager.default.addPanGestureToPresent(toView: self.navigationController!.navigationBar)
        SideMenuManager.default.addScreenEdgePanGesturesToPresent(toView: self.navigationController!.view)
        
        menuRightNavigationController.settings.statusBarEndAlpha = 0.0
        menuRightNavigationController.settings.menuWidth = 300
    }
    
}

// MARK: ==================== TABLEVIEW EXTENSION ===========================
extension HomeVC: UITableViewDataSource, UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arrMenu.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tblMain.dequeueReusableCell(withIdentifier: "HomeTC") as! HomeTC
        cell.selectionStyle = .none
        
        cell.lblStatus.text = "Status: In Progress * Date: 12 Nov 2019"
        
        let attrs1 = [NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 14), NSAttributedString.Key.foregroundColor : UIColor.blue]
        let attrs2 = [NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 14), NSAttributedString.Key.foregroundColor : UIColor.yellow]
        let attrs3 = [NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 14), NSAttributedString.Key.foregroundColor : UIColor.blue]
        
        let attributedString1 = NSMutableAttributedString(string:"Status: ", attributes:attrs1)
        
        let attributedString2 = NSMutableAttributedString(string:"In Progress", attributes:attrs2)
        
        let attributedString3 = NSMutableAttributedString(string:" * Date: 12 Nov 2019", attributes:attrs3)
        
        attributedString1.append(attributedString2)
        attributedString1.append(attributedString3)
        
        cell.lblStatus.attributedText = attributedString1
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 60
    }
}
