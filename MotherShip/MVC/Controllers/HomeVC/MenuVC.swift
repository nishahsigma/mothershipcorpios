//
//  MenuVC.swift
//  MotherShip
//
//  Created by Admin on 04/03/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit
import SideMenu

class MenuVC: UIViewController ,HomeProtocol{

    @IBOutlet weak var tblMain: UITableView!
    
    @IBOutlet weak var imgUser: UIImageView!
    
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblVersion: UILabel!
    
   
    var arrMenu = NSArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
       
        arrMenu = [1,2,3]
    }
    
    @IBAction func btnSettingClicked(_ sender: Any) {
        
    }
    
    @IBAction func btnContactUsClicked(_ sender: Any) {
           
       }
    
    @IBAction func btnPrivacyPolicyClicked(_ sender: Any) {
           
       }
    
    @IBAction func btnTermConditionClicked(_ sender: Any) {
           
       }
    
    @IBAction func btnLogoutClicked(_ sender: Any) {
           logOut()
    }
}

// MARK: ==================== TABLEVIEW EXTENSION ===========================
extension MenuVC: UITableViewDataSource, UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arrMenu.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tblMain.dequeueReusableCell(withIdentifier: "MenuTC") as! MenuTC
        cell.selectionStyle = .none
        
        cell.lblMain.text = "Profile"
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 45
    }
}

