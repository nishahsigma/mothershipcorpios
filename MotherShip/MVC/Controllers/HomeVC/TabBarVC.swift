//
//  TabBarVC.swift
//  MotherShip
//
//  Created by Admin on 04/03/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

class TabBarVC: UITabBarController,HomeProtocol{
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        UINavigationBar.appearance().tintColor = .white
        UINavigationBar.appearance().isTranslucent = true
        let attributes = [NSAttributedString.Key.font: UIFont.customBold(size: 20),
                          NSAttributedString.Key.foregroundColor : UIColor.white]
        
        UINavigationBar.appearance().titleTextAttributes = attributes
        
        UINavigationBar.appearance().setBackgroundImage(UIImage(), for: .default)
        UINavigationBar.appearance().shadowImage = UIImage()
        UINavigationBar.appearance().backgroundColor = UIColor.clear
        UINavigationBar.appearance().isTranslucent = true
        
        setTabBar()
        
    }
    
    
    func setTabBar(){
        UITabBar.appearance().barTintColor =  UIColor(red: 21.0/255.0, green: 50.0/255.0, blue: 114.0/255.0, alpha: 1.0)
        tabBar.layer.shadowColor = UIColor.black.cgColor
        tabBar.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        tabBar.layer.shadowRadius = 5
        tabBar.layer.shadowOpacity = 1
        tabBar.layer.masksToBounds = false
        
        for item in (tabBar.items)!{
            if item.title == "Home"{
                
                item.image = UIImage(named: "imgHome")?.withRenderingMode(.alwaysOriginal)
                item.selectedImage = UIImage(named: "imgHomeSelected")?.withRenderingMode(.alwaysOriginal)
            } else if item.title == "Inbox"{
                item.image = UIImage(named: "imgInbox")?.withRenderingMode(.alwaysOriginal)
                item.selectedImage = UIImage(named: "imgInboxSelected")?.withRenderingMode(.alwaysOriginal)
            } else if item.title == ""{
                item.image = UIImage(named: "add")?.withRenderingMode(.alwaysOriginal)
                item.selectedImage = UIImage(named: "add")?.withRenderingMode(.alwaysOriginal)
            } else if item.title == "Notification"{
                item.image = UIImage(named: "imgNotification")?.withRenderingMode(.alwaysOriginal)
                item.selectedImage = UIImage(named: "imgNotificationSelected")?.withRenderingMode(.alwaysOriginal)
            } else if item.title == "Account"{
                item.image = UIImage(named: "imgAccount")?.withRenderingMode(.alwaysOriginal)
                item.selectedImage = UIImage(named: "imgAccountSelected")?.withRenderingMode(.alwaysOriginal)
            }
        }
        
        let selectedColor   = UIColor.white//UIColor(red: 246.0/255.0, green: 155.0/255.0, blue: 13.0/255.0, alpha: 1.0)
        let unselectedColor = UIColor.tabBarTextColor//UIColor(red: 16.0/255.0, green: 224.0/255.0, blue: 223.0/255.0, alpha: 1.0)
        
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: unselectedColor], for: .normal)
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: selectedColor], for: .selected)
     }
    
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        
        print(tabBar.selectedItem?.title)
        
        if tabBar.selectedItem?.title == ""{
            
            let objVC =  BookServiceVC.getViewController() as! BookServiceVC
            
            self.navigationController?.pushViewController(objVC, animated: true)
        }
    }
}

extension UITabBarController {
    
    func setBadges(badgeValues: [Int]) {
        
        for view in self.tabBar.subviews {
            if view is CustomTabBadge {
                view.removeFromSuperview()
            }
        }
        
        for index in 0...badgeValues.count-1 {
            if badgeValues[index] != 0 {
                addBadge(index: index, value: badgeValues[index], color:UIColor.appBlue, font: UIFont.custom(size: 10))
            }
        }
    }
    
    func addBadge(index: Int, value: Int, color: UIColor, font: UIFont) {
        let badgeView = CustomTabBadge()
        
        badgeView.clipsToBounds = true
        badgeView.textColor = .appBlue
        badgeView.textAlignment = .center
        badgeView.font = font
        badgeView.text = String(value)
        badgeView.backgroundColor = color
        badgeView.tag = index
        tabBar.addSubview(badgeView)
        
        self.positionBadges()
    }
    
    override open func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.positionBadges()
    }
    
    // Positioning
    func positionBadges() {
        
        var tabbarButtons = self.tabBar.subviews.filter { (view: UIView) -> Bool in
            return view.isUserInteractionEnabled // only UITabBarButton are userInteractionEnabled
        }
        
        tabbarButtons = tabbarButtons.sorted(by: { $0.frame.origin.x < $1.frame.origin.x })
        
        for view in self.tabBar.subviews {
            if view is CustomTabBadge {
                let badgeView = view as! CustomTabBadge
                self.positionBadge(badgeView: badgeView, items:tabbarButtons, index: badgeView.tag)
            }
        }
    }
    
    func positionBadge(badgeView: UIView, items: [UIView], index: Int) {
        
        let itemView = items[index]
        let center = itemView.center
        
        let xOffset: CGFloat = 12
        let yOffset: CGFloat = -14
        badgeView.frame.size = CGSize(width:17,height: 17)
        badgeView.center = CGPoint(x: center.x + xOffset,y: center.y + yOffset)
        badgeView.layer.cornerRadius = badgeView.bounds.width/2
        tabBar.bringSubviewToFront(badgeView)
    }
}

class CustomTabBadge: UILabel {}



