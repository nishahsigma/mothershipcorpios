//
//  BookServiceVC.swift
//  MotherShip
//
//  Created by Admin on 06/03/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

class BookServiceVC: UIViewController, HomeProtocol {
    
    var dropDown = DropDown()
    var btnMorerOption = UIBarButtonItem()
    
    override func viewDidLoad() {
        super.viewDidLoad()
     
        

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
        
        setNavigationButton()
        setDropDown()
    }
    
    @IBAction func btnCreationStationClicked(_ sender: Any) {
        
        let objVC =  CreateStationVC.getViewController() as! CreateStationVC
        self.navigationController?.pushViewController(objVC, animated: true)
    }
    
    @IBAction func btnPodStationClicked(_ sender: Any) {
        
    }
    
    @IBAction func btnPodStation20Clicked(_ sender: Any) {
        
    }
    
    @IBAction func btnSuperspedeClicked(_ sender: Any) {
        
    }
    
    
    // MARK: ==================== FUNCTION ===========================
    
    func setNavigationButton(){
        
        btnMorerOption = UIBarButtonItem(image: UIImage(named: "imgMore"), style: .done, target: self, action: #selector(btnMoreOptionClicked))
        
        let label = UILabel()
        label.font = .custom(size: 20)
        label.textColor = .white
        label.text = "Book a Service"
        label.textAlignment = .left
        
       // let titleItem = UIBarButtonItem(customView: label)
        navigationItem.rightBarButtonItems = [btnMorerOption]
        
        navigationItem.titleView = label
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
    }
    
    func setDropDown(){
       
        dropDown = DropDown()
        dropDown.anchorView = btnMorerOption
        
        dropDown.dataSource = ["Membership", "Create Project", "Learn More"]
        
//        if let frame = self.navigationItem.rightBarButtonItems?.first?.frame {
//            dropDown.bottomOffset = CGPoint(x: 200, y: frame.width - 100)
//        }
        
        dropDown.cellNib = UINib(nibName: "MyCell", bundle: nil)
        dropDown.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
            guard let cell = cell as? MyCell else { return }
            
            cell.logoImageView.image = UIImage(named: "info\(index)")
        }
        
        DropDown.appearance().textColor = UIColor.white
        DropDown.appearance().selectedTextColor = UIColor.lightGrayText
        DropDown.appearance().textFont = UIFont.systemFont(ofSize: 15)
        DropDown.appearance().backgroundColor = UIColor.appBlue
        DropDown.appearance().selectionBackgroundColor = UIColor.tabBarBackground
        DropDown.appearance().cellHeight = 40
        DropDown.appearance().cornerRadius = 10
        dropDown.width = 200
        
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
          print("Selected item: \(item) at index: \(index)")
            
            if index == 0{
                
            }
            else if index == 1{
                
                let objVC =  CreateProjectVC.getViewController() as! CreateProjectVC
                self.navigationController?.pushViewController(objVC, animated: true)
                
            }else{
                
            }
            
        }
    }
    
    @objc func btnMoreOptionClicked(){
        dropDown.show()
    }
    
}
