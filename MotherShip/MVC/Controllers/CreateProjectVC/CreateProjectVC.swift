//
//  CreateProjectVC.swift
//  MotherShip
//
//  Created by Admin on 12/03/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

class CreateProjectVC: UIViewController, HomeProtocol {
    
    @IBOutlet weak var txtExistingProject: customTextField!
    @IBOutlet weak var txtStartNewProject: customTextField!
    @IBOutlet weak var txtSpecificProvider: customTextField!
    
    @IBOutlet weak var btnSkipExisting: UIButton!
    @IBOutlet weak var btnSkipStartNewProject: UIButton!
    @IBOutlet weak var btnSkipSpecificProvider: UIButton!
    
    @IBOutlet weak var btnAddExisting: UIButton!
    @IBOutlet weak var btnAddStartNewProject: UIButton!
    @IBOutlet weak var btnAddSpecificProvider: UIButton!
    
    var dropDown = DropDown()
    var btnMorerOption = UIBarButtonItem()
    
    override func viewDidLoad(){
        super.viewDidLoad()
       
        title = "Create Project"
        setLayout()
        setNavigationButton()
        setDropDown()
    }
    
    // MARK: ==================== FUNCTION ===========================
    func setNavigationButton(){
        
        btnMorerOption = UIBarButtonItem(image: UIImage(named: "imgMore"), style: .done, target: self, action: #selector(btnMoreOptionClicked))
        
        let label = UILabel()
        label.font = .custom(size: 20)
        label.textColor = .white
        label.text = "Create Project"
        label.textAlignment = .left
        
        //  let titleItem = UIBarButtonItem(customView: label)
        navigationItem.rightBarButtonItems = [btnMorerOption]
        
        navigationItem.titleView = label
    }
    
    func setDropDown(){
        
        dropDown = DropDown()
        dropDown.anchorView = btnMorerOption
        
        dropDown.dataSource = ["Membership", "Create Project", "Learn More"]
        
        //        if let frame = self.navigationItem.rightBarButtonItems?.first?.frame {
        //            dropDown.bottomOffset = CGPoint(x: 200, y: frame.width - 100)
        //        }
        
        dropDown.cellNib = UINib(nibName: "MyCell", bundle: nil)
        dropDown.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
            guard let cell = cell as? MyCell else { return }
            
            cell.logoImageView.image = UIImage(named: "info\(index)")
        }
        
        DropDown.appearance().textColor = UIColor.white
        DropDown.appearance().selectedTextColor = UIColor.lightGrayText
        DropDown.appearance().textFont = UIFont.systemFont(ofSize: 15)
        DropDown.appearance().backgroundColor = UIColor.appBlue
        DropDown.appearance().selectionBackgroundColor = UIColor.tabBarBackground
        DropDown.appearance().cellHeight = 40
        DropDown.appearance().cornerRadius = 10
        dropDown.width = 200
        
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            
            if index == 0{
                
            }
            else if index == 1{
                
                let objVC =  CreateProjectVC.getViewController() as! CreateProjectVC
                self.navigationController?.pushViewController(objVC, animated: true)
                
            }else{
                
            }
        }
    }
        
        // MARK: ==================== BUTTON CLICK ===========================
        @objc func btnMoreOptionClicked(){
            dropDown.show()
        }
        
        @IBAction func btnSkipExistingClicked(_ sender: Any) {
            
        }
        
        @IBAction func btnSkipStartNewProjectClicked(_ sender: Any) {
            
        }
        
        @IBAction func btnSkipSpecificProviderClicked(_ sender: Any) {
            
        }
        
        @IBAction func btnAddExistingClicked(_ sender: Any) {
            
        }
        
        @IBAction func btnAddStartNewProjectClicked(_ sender: Any) {
            
        }
        
        @IBAction func btnAddSpecificProviderClicked(_ sender: Any) {
            
        }
        
        // MARK: ==================== FUNCTION ===========================
        
        func setLayout(){
            
            btnAddExisting.layer.cornerRadius = 5
            btnAddStartNewProject.layer.cornerRadius = 5
            btnAddSpecificProvider.layer.cornerRadius = 5
            
            btnAddExisting.backgroundColor = UIColor.buttonBackground
            btnAddStartNewProject.backgroundColor = UIColor.buttonBackground
            btnAddSpecificProvider.backgroundColor = UIColor.buttonBackground
            
            txtExistingProject.attributedPlaceholder = NSAttributedString(string:"Enter Project ID", attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
            txtStartNewProject.attributedPlaceholder = NSAttributedString(string:"Enter Project Name", attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
            txtSpecificProvider.attributedPlaceholder = NSAttributedString(string:"Enter Provider Email ID", attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        }
    }
