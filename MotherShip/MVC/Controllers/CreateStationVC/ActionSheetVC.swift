//
//  ActionSheetVC.swift
//  MotherShip
//
//  Created by Admin on 16/03/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

class ActionSheetVC: UIViewController, CreateStationProtocol {
    
    @IBOutlet weak var tblMain: UITableView!
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnCancel: UIButton!
    
    var arrList = NSArray()
    var intType = Int()
    
    var strSelectedItem: ((String) -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblMain.tableFooterView = UIView()
        
        setData()
        
        
        
        btnCancel.layer.cornerRadius = 5.0
        tblMain.layer.cornerRadius = 5.0
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnCancelClicked(_ sender: Any) {
        
        dismiss(animated: true, completion: nil)
    }
    
    func setData(){
        
        if intType == 1{
            arrList = ["ARCHITECTURAL DRAWINGS","BLOG EXECUTION","BRANDING","CASTING (FILM/DIGITAL/TV)","CODING (WEB/DIGITAL)","COPY/CREATIVE WRITING","CREATIVE DIRECTION","DIGITAL MEDIA CONTENT CREATION","DIRECTING (FILM/DIGITAL/TV)","GOOGLE ADWORDS","GRAPHIC DESIGN","MARKETING (MANAGEMENT)","MARKETING (STRATEGY)","PACKAGING DESIGN","PHOTOGRAPHY (FILM/DIGITAL/TV)","PRINT PRODUCTION","PRODUCT DESIGN","PRODUCT DEVELOPMENT","PRODUCTION (FILM/DIGITAL/TV)","PUBLIC RELATIONS","SCRIPT WRITING (FILM/DIGITAL/TV)","SEO","SOCIAL MEDIA CONTENT CREATION","SOCIAL MEDIA MANAGEMENT","VIDEOGRAPHY (FILM/DIGITAL/TV)","VIDEOS/ANIMATION","VLOG PRODUCTION","WEB DESIGN (UI/UX)","WEB DEVELOPMENT"]
            
            lblTitle.text = "Creative Counter"
            
        }else if intType == 2{
            arrList = ["RISING STAR CATEGORY $25/HR","GIANT STAR CATEGORY $50/HR","SUPER GIANT STAR CATEGORY $150/HR","HIPER GIANT CATEGORY $300/HR","SUPER STAR CATEGORY $1000/HR"]
            
            lblTitle.text = "Level"
            
        }else if intType == 3{
            arrList = ["COMMANDER - 20% OFF","PILOT - 15% OFF","SPACE FLIGHT"]
            
            lblTitle.text = "Commander"
            
        }else if intType == 4{
            
            arrList = ["ACCOUNTING","ADMINISTRATIVE SERVICES AND COORDINATORS","BUSINESS ADVICE","BUSINESS PLANS","C-LEVEL EXECUTIVES","CEO TO GO","CMO TO GO","FINANCE/PROJECTIONS","GENERAL ACCOUNTING (P&L/BUDGETS)","LEGAL ADVICE/CONTRACTS","PROJECT MANAGEMENT","RESUME WRITING","SALES WRITING","TRADEMARKING/PATENTS","VENTURE CAPITAL CONSULTING","VIRTUAL ASSISTANT"]
            
            lblTitle.text = "Business Bar"
            
        }else if intType == 5{
             arrList = ["Rising Star Category $25/hr","Gaint Category $50/hr","Super Gaint Category $150/hr","Hyper Gaint Category $300/hr","uper Star Category $1000/hr"]
             lblTitle.text = "Level"
        }else if intType == 6{
            arrList = ["Commander-20% Off","Pilot-15% Off","Space Flight"]
            
            lblTitle.text = "Discount"
        }else{
            
            
        }
        
    }
}

// MARK: ==================== TABLEVIEW METHOD ===========================
extension ActionSheetVC: UITableViewDataSource, UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arrList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tblMain.dequeueReusableCell(withIdentifier: "ActionSheetTC", for: indexPath) as! ActionSheetTC
        cell.selectionStyle = .none
        
        cell.lblMain.text = arrList[indexPath.row] as? String
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        strSelectedItem!(arrList[indexPath.row] as! String)
        dismiss(animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 50
    }
}
