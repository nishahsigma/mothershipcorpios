//
//  CreateStationVC.swift
//  MotherShip
//
//  Created by Admin on 12/03/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

class CreateStationVC: UIViewController, CreateStationProtocol {
    
    @IBOutlet weak var sagmentViewOption: TTSegmentedControl!
    
    @IBOutlet weak var viewCreativeCounter: UIView!
    @IBOutlet weak var viewBusinessBar: UIView!
    
    @IBOutlet weak var txtBlogExecution: customTextField!
    @IBOutlet weak var txtGiantLevel: customTextField!
    @IBOutlet weak var txtCommander: customTextField!
    @IBOutlet weak var txtHours: customTextField!
    @IBOutlet weak var txtMoney: customTextField!
    
    @IBOutlet weak var txtBusinessBlogExecution: customTextField!
    @IBOutlet weak var txtBusinessGiantLevel: customTextField!
    @IBOutlet weak var txtBusinessCommander: customTextField!
    @IBOutlet weak var txtBusinessHours: customTextField!
    @IBOutlet weak var txtBusinessMoney: customTextField!
    
    @IBOutlet weak var btnBookService: UIButton!
    @IBOutlet weak var btnBusinessBookService: UIButton!
    
    var dropDown = DropDown()
    var btnMorerOption = UIBarButtonItem()
    
    var intHour = 50
    var intDisscount = 20
    var intTotal     = Int()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setLayoutCreateiveCounter()
        setLyoutTongleButton()
        setNavigationButton()
        setDropDown()
    }
    
    // MARK: ==================== FUNCTION ===========================
    func setLyoutTongleButton(){
        sagmentViewOption.itemTitles = ["Creative Counter","Business Bar"]
        sagmentViewOption.defaultTextFont = UIFont(name: "Montserrat-Regular", size: 14)!
        sagmentViewOption.selectedTextFont = UIFont(name: "Montserrat-Regular", size: 14)!
        sagmentViewOption.thumbGradientColors = [UIColor.blue,UIColor.lightGray]
        sagmentViewOption.allowChangeThumbWidth = false
        sagmentViewOption.frame = CGRect(x: 10, y: 10, width: 50, height: 50)
        sagmentViewOption.containerBackgroundColor = UIColor.sagmantegControlBG
        sagmentViewOption.thumbColor = UIColor.buttonBackground
        
        sagmentViewOption.didSelectItemWith = { (index, title) -> () in
            
            if index == 0{
                self.viewCreativeCounter.isHidden = false
                self.viewBusinessBar.isHidden = true
            } else {
                self.viewCreativeCounter.isHidden = true
                self.viewBusinessBar.isHidden = false
            }
        }
    }
    
    func setDropDown(){
        
        dropDown = DropDown()
        dropDown.anchorView = btnMorerOption
        
        dropDown.dataSource = ["Membership", "Learn More"]
        
        //        if let frame = self.navigationItem.rightBarButtonItems?.first?.frame {
        //            dropDown.bottomOffset = CGPoint(x: 200, y: frame.width - 100)
        //        }
        
        dropDown.cellNib = UINib(nibName: "MyCell", bundle: nil)
        dropDown.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
            guard let cell = cell as? MyCell else { return }
            
            cell.logoImageView.image = UIImage(named: "info\(index)")
        }
        
        DropDown.appearance().textColor = UIColor.white
        DropDown.appearance().selectedTextColor = UIColor.lightGrayText
        DropDown.appearance().textFont = UIFont.systemFont(ofSize: 15)
        DropDown.appearance().backgroundColor = UIColor.appBlue
        DropDown.appearance().selectionBackgroundColor = UIColor.tabBarBackground
        DropDown.appearance().cellHeight = 40
        DropDown.appearance().cornerRadius = 10
        dropDown.width = 200
        
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            
            if index == 0{
                
            }else{
                
            }
        }
    }
    
    func setNavigationButton(){
        
        btnMorerOption = UIBarButtonItem(image: UIImage(named: "imgMore"), style: .done, target: self, action: #selector(btnMoreOptionClicked))
        
        let label = UILabel()
        label.font = .custom(size: 20)
        label.textColor = .white
        label.text = "Creation Station"
        label.textAlignment = .left
        
        //  let titleItem = UIBarButtonItem(customView: label)
        navigationItem.rightBarButtonItems = [btnMorerOption]
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)

        navigationItem.titleView = label
    }
    
    func setLayoutCreateiveCounter(){
        
        btnBookService.layer.cornerRadius = 5.0
        btnBookService.backgroundColor = .buttonBackground
        txtHours.attributedPlaceholder = NSAttributedString(string:"Hours", attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGrayText])
        txtMoney.attributedPlaceholder = NSAttributedString(string:"$xxx.xx", attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGrayText])
        
        btnBusinessBookService.layer.cornerRadius = 5.0
        btnBusinessBookService.backgroundColor = .buttonBackground
        txtBusinessHours.attributedPlaceholder = NSAttributedString(string:"Hours", attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGrayText])
        txtBusinessMoney.attributedPlaceholder = NSAttributedString(string:"$xxx.xx", attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGrayText])
        
        txtHours.paddingLeft = 5
        txtMoney.paddingLeft = 5
        
        self.txtMoney.text = "$\(String(self.getTotal(hours: self.intHour, disscount: self.intDisscount, userHours: 0)))"
    }
    
    // MARK: ==================== BUTTON CLICK ===========================
    @objc func btnMoreOptionClicked(){
        dropDown.show()
    }
    
    @IBAction func btnBookServiceClicked(_ sender: Any) {
        
        let objVC =  ProviderDetailsVC.getViewController() as! ProviderDetailsVC
        self.navigationController?.pushViewController(objVC, animated: true)
    }
    
    @IBAction func btnBusinessBookServiceClicked(_ sender: Any) {
        
        
    }
    
    func getTotal(hours:Int,disscount:Int,userHours:Int) -> Int{
        
        var intFinalTotal = Int()
        
        if userHours != 0{
            if disscount != 0{
                let mainAmoint = hours * disscount / 100
                print(mainAmoint)
                
                intFinalTotal = mainAmoint * userHours
                print(intFinalTotal)
            }else{
                intFinalTotal = hours * userHours
                print(intFinalTotal)
            }
        }
        
        return intFinalTotal
    }
}

// MARK: ==================== EXTENSION ===========================
extension CreateStationVC: UITextFieldDelegate{
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == txtBlogExecution{
            
            let objVC =  ActionSheetVC.getViewController() as! ActionSheetVC
            objVC.intType = 1
            objVC.modalPresentationStyle = .overCurrentContext
            objVC.strSelectedItem = { strTitle in
                
                self.txtBlogExecution.text = strTitle
            }
            self.present(objVC, animated: true, completion: nil)
            
            return false
        }else  if textField == txtGiantLevel{
            
            let objVC =  ActionSheetVC.getViewController() as! ActionSheetVC
            objVC.intType = 2
            objVC.modalPresentationStyle = .overCurrentContext
            objVC.strSelectedItem = { strTitle in
                
                self.txtGiantLevel.text = strTitle
                
                let result = strTitle.split(separator: "$")
                print(result)
                
                let result1 = result[1].split(separator: "/")
                print(result1[0])
                
                self.intHour = Int(result1[0].description)!
                print(self.intHour)
                
                if !self.txtHours.text!.isEmpty{
                    self.txtMoney.text = "$\(String(self.getTotal(hours: self.intHour, disscount: self.intDisscount, userHours: Int(self.txtHours.text!)!)))"
                }
                
            }
            self.present(objVC, animated: true, completion: nil)
            
            return false
        }else  if textField == txtCommander{
            
            let objVC =  ActionSheetVC.getViewController() as! ActionSheetVC
            objVC.intType = 3
            objVC.modalPresentationStyle = .overCurrentContext
            objVC.strSelectedItem = { strTitle in
                
                self.txtCommander.text = strTitle
                
                if strTitle.contains("-"){
                    let result = strTitle.split(separator: "-")
                    print(result)
                    
                    let result1 = result[1].split(separator: "%")
                    print(result1[0])
                    
                    let trimmedString = result1[0].description.trimmingCharacters(in: .whitespaces)
                    
                    self.intDisscount = Int(trimmedString)!
                    print(self.intDisscount)
                    
                    if !self.txtHours.text!.isEmpty{
                        self.txtMoney.text = "$\(String(self.getTotal(hours: self.intHour, disscount: self.intDisscount, userHours: Int(self.txtHours.text!)!)))"
                    }
                }else{
                    self.intDisscount = 0
                    if !self.txtHours.text!.isEmpty{
                        self.txtMoney.text = "$\(String(self.getTotal(hours: self.intHour, disscount: self.intDisscount, userHours: Int(self.txtHours.text!)!)))"
                    }
                }
            }
            
            self.present(objVC, animated: true, completion: nil)
            
            return false
        }else  if textField == txtBusinessBlogExecution{
            
            let objVC =  ActionSheetVC.getViewController() as! ActionSheetVC
            objVC.intType = 4
            objVC.modalPresentationStyle = .overCurrentContext
            objVC.strSelectedItem = { strTitle in
                
                self.txtBusinessBlogExecution.text = strTitle
            }
            self.present(objVC, animated: true, completion: nil)
            
            return false
        }else  if textField == txtBusinessGiantLevel{
            
            let objVC =  ActionSheetVC.getViewController() as! ActionSheetVC
            objVC.intType = 5
            objVC.modalPresentationStyle = .overCurrentContext
            objVC.strSelectedItem = { strTitle in
                
                self.txtBusinessGiantLevel.text = strTitle
            }
            self.present(objVC, animated: true, completion: nil)
            
            return false
        }else  if textField == txtBusinessCommander{
            
            let objVC =  ActionSheetVC.getViewController() as! ActionSheetVC
            objVC.intType = 6
            objVC.modalPresentationStyle = .overCurrentContext
            objVC.strSelectedItem = { strTitle in
                
                self.txtBusinessCommander.text = strTitle
            }
            self.present(objVC, animated: true, completion: nil)
            
            return false
        }else{
            return true
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == txtHours{
            
            let currentString: NSString = textField.text! as NSString
            let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
            
            if !newString.description.isEmpty{
                txtMoney.text = "$\(String(getTotal(hours: intHour, disscount: intDisscount, userHours: Int(newString.description)!)))"
            }else{
                txtMoney.text = "$\(String(getTotal(hours: intHour, disscount: intDisscount, userHours: 0)))"
            }
            
            return true
        }else{
            return true
        }
    }
    
}
