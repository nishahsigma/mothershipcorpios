//
//  ProviderDetailsVC.swift
//  MotherShip
//
//  Created by Admin on 17/03/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

class ProviderDetailsVC: UIViewController, CreateStationProtocol {
    
    @IBOutlet weak var lblVirtual: UILabel!
    @IBOutlet weak var lblPerson: UILabel!
    @IBOutlet weak var lblPhoneNumber: UILabel!
    @IBOutlet weak var lblEmailAddress: UILabel!
    
    @IBOutlet weak var txtPhoneNumber: customTextField!
    @IBOutlet weak var txtEmailAddress: customTextField!
    
    @IBOutlet weak var imgMothtershipVideo: UIImageView!
    @IBOutlet weak var imgSkype: UIImageView!
    @IBOutlet weak var imgGoogleHangout: UIImageView!
    @IBOutlet weak var imgComeToMe: UIImageView!
    @IBOutlet weak var imgiWIllComeToYou: UIImageView!
    
    @IBOutlet weak var btnMothershipVideo: UIButton!
    @IBOutlet weak var btnSkype: UIButton!
    @IBOutlet weak var btnHangout: UIButton!
    @IBOutlet weak var btnComeToMe: UIButton!
    @IBOutlet weak var btnIWillComeToYou: UIButton!
    @IBOutlet weak var btnSubmitPhone: UIButton!
    @IBOutlet weak var btnSubmitEmail: UIButton!
    
    @IBOutlet weak var conPhoneViewHeight: NSLayoutConstraint!
    @IBOutlet weak var conEmailViewHeight: NSLayoutConstraint!
    
    
    var dropDown = DropDown()
    var btnMorerOption = UIBarButtonItem()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setDropDown()
        setNavigationButton()
        setBullateLable()
    }
    
    // MARK: ==================== BUTTON CLICK ===========================
    @IBAction func btnMothershipVideoClicked(_ sender: UIButton) {
        isSelect(tag: sender.tag)
    }
    @IBAction func btnSkypeClicked(_ sender: UIButton) {
        isSelect(tag: sender.tag)
    }
    @IBAction func btnHangoutClicked(_ sender: UIButton) {
        isSelect(tag: sender.tag)
    }
    @IBAction func btnComeToMeClicked(_ sender: UIButton) {
        isSelect(tag: sender.tag)
    }
    @IBAction func btnIWillComeToYouClicked(_ sender: UIButton) {
        isSelect(tag: sender.tag)
    }
    
    @IBAction func btnSubmitPhoneClicked(_ sender: UIButton) {
        
    }
    
    @IBAction func btnSubmitEmailClicked(_ sender: UIButton) {
        
    }
    
    @objc func btnMoreOptionClicked(){
        dropDown.show()
    }
    
    // MARK: ==================== FUNCTION ===========================
    func setNavigationButton(){
        
        btnMorerOption = UIBarButtonItem(image: UIImage(named: "imgMore"), style: .done, target: self, action: #selector(btnMoreOptionClicked))
        
        let label = UILabel()
        label.font = .custom(size: 20)
        label.textColor = .white
        label.text = "Creation Station"
        label.textAlignment = .left
        
        //  let titleItem = UIBarButtonItem(customView: label)
        navigationItem.rightBarButtonItems = [btnMorerOption]
        
        navigationItem.titleView = label
        
        btnSubmitPhone.layer.cornerRadius = 5
        btnSubmitEmail.layer.cornerRadius = 5
        
        conPhoneViewHeight.constant = 100
        conEmailViewHeight.constant = 100
        
        btnSubmitPhone.isHidden = true
        btnSubmitEmail.isHidden = true
    }
    
    @objc func isSelect(tag:Int){
        
        switch tag {
        case 1:
            imgMothtershipVideo.image = UIImage(named: "imgCheck")
            imgSkype.image = UIImage(named: "imgUncheck")
            imgGoogleHangout.image = UIImage(named: "imgUncheck")
            
            let objVC = AlertMothershipVideoVC.getViewController() as! AlertMothershipVideoVC
            objVC.modalPresentationStyle = .overCurrentContext
            objVC.intPresent = 1
            present(objVC, animated: true, completion: nil)
            
        case 2:
            imgMothtershipVideo.image = UIImage(named: "imgUncheck")
            imgSkype.image = UIImage(named: "imgCheck")
            imgGoogleHangout.image = UIImage(named: "imgUncheck")
            
            let objVC = AlertMothershipVideoVC.getViewController() as! AlertMothershipVideoVC
            objVC.modalPresentationStyle = .overCurrentContext
            objVC.intPresent = 2
            present(objVC, animated: true, completion: nil)
        case 3:
            imgMothtershipVideo.image = UIImage(named: "imgUncheck")
            imgSkype.image = UIImage(named: "imgUncheck")
            imgGoogleHangout.image = UIImage(named: "imgCheck")
            
            let objVC = AlertMothershipVideoVC.getViewController() as! AlertMothershipVideoVC
            objVC.modalPresentationStyle = .overCurrentContext
            objVC.intPresent = 3
            present(objVC, animated: true, completion: nil)
        case 4:
            imgComeToMe.image = UIImage(named: "imgCheck")
            imgiWIllComeToYou.image = UIImage(named: "imgUncheck")
            
            let objVC = AlertComeToMeVC.getViewController() as! AlertComeToMeVC
            objVC.modalPresentationStyle = .overCurrentContext
            present(objVC, animated: true, completion: nil)
        case 5:
            imgComeToMe.image = UIImage(named: "imgUncheck")
            imgiWIllComeToYou.image = UIImage(named: "imgCheck")
            
            let objVC = AlertIWillComeToYouSelectStoreVC.getViewController() as! AlertIWillComeToYouSelectStoreVC
            objVC.modalPresentationStyle = .overCurrentContext
            present(objVC, animated: true, completion: nil)
        default:
            print("")
        }
    }
    
    func setBullateLable(){
        
        lblVirtual.text = "\u{2022} Virtual (Online Video Chat)"
        lblPerson.text = "\u{2022} In Person"
        lblPhoneNumber.text = "\u{2022} Phone Number"
        lblEmailAddress.text = "\u{2022} Email Address"
    }
    
    func setDropDown(){
        
        dropDown = DropDown()
        dropDown.anchorView = btnMorerOption
        
        dropDown.dataSource = ["Membership", "Learn More"]
        
        //        if let frame = self.navigationItem.rightBarButtonItems?.first?.frame {
        //            dropDown.bottomOffset = CGPoint(x: 200, y: frame.width - 100)
        //        }
        
        dropDown.cellNib = UINib(nibName: "MyCell", bundle: nil)
        dropDown.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
            guard let cell = cell as? MyCell else { return }
            
            cell.logoImageView.image = UIImage(named: "info\(index)")
        }
        
        DropDown.appearance().textColor = UIColor.white
        DropDown.appearance().selectedTextColor = UIColor.lightGrayText
        DropDown.appearance().textFont = UIFont.systemFont(ofSize: 15)
        DropDown.appearance().backgroundColor = UIColor.appBlue
        DropDown.appearance().selectionBackgroundColor = UIColor.tabBarBackground
        DropDown.appearance().cellHeight = 40
        DropDown.appearance().cornerRadius = 10
        dropDown.width = 200
        
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            
            if index == 0{
                
            }else{
                
            }
        }
    }
    
}

// MARK: ==================== EXTENSION TEXTFIELD METHOD ===========================
extension ProviderDetailsVC: UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let currentString: NSString = textField.text! as NSString
        let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
        
        if textField == txtPhoneNumber{
            if (newString.description.count>=10){
                UIView.transition(with: view, duration: 0.5, options: .transitionCrossDissolve, animations: {
                    self.conPhoneViewHeight.constant = 130
                    self.btnSubmitPhone.isHidden = false
                })
            }else{
                conPhoneViewHeight.constant = 100
                btnSubmitPhone.isHidden = true
            }
            return true
        }else if textField == txtEmailAddress{
            
            let isEmailValid = Helper.isValidEmail(newString.description)
            if isEmailValid{
                UIView.transition(with: view, duration: 0.5, options: .transitionCrossDissolve, animations: {
                    self.conEmailViewHeight.constant = 130
                    self.btnSubmitEmail.isHidden = false
                })
            }else{
                self.conEmailViewHeight.constant = 100
                self.btnSubmitEmail.isHidden = true
            }
            return true
        }else{
            return true
        }
    }
}
