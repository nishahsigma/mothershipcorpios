//
//  BookserviceProviderVC.swift
//  MotherShip
//
//  Created by keval dattani on 19/03/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit
import HTagView

class BookserviceProviderVC: UIViewController,CreateStationProtocol,HTagViewDataSource,HTagViewDelegate {
    
    var btnMorerOption = UIBarButtonItem()
    
    @IBOutlet weak var btnLunch: UIButton!
    @IBOutlet weak var firstTagview: HTagView!
    @IBOutlet weak var secondTagview: HTagView!
    @IBOutlet weak var ThirdTagview: HTagView!
    let first = ["Happy","Delignet","Funny","Fast talker","Meduim talker","Enter custom field","Serious","Any"]
    
    let second = ["Cutting Edge","Traditional","Commercial","Residential","Fashion Foward","Corporate","Retail","Enter custom field","Wholesale","Any"]
    
    let third = ["Banking","Consumer product","Finance","Fashion","Politics","Farming and agriculture","Luxury","Design","Enter custom field","Goverment","Education","Wellness","Architecture"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setNavigationButton()
        firstTagview.delegate = self
        secondTagview.delegate = self
        ThirdTagview.delegate = self
        
        firstTagview.dataSource = self
        secondTagview.dataSource = self
        ThirdTagview.dataSource = self
        
        firstTagview.tagBorderColor = UIColor.white.cgColor
        firstTagview.tagBorderWidth = 1.0
        firstTagview.tagMainBackColor = UIColor.buttonBackground
        firstTagview.tagSecondBackColor = UIColor.clear
        firstTagview.tagSecondTextColor = UIColor.white
        firstTagview.tagFont = UIFont(name: "Montserrat-Regular", size: 14.0)!
        firstTagview.tagContentEdgeInsets = UIEdgeInsets(top: 10, left: 20, bottom: 10, right: 20)
        firstTagview.tagMaximumWidth = .HTagAutoMaximumWidth
        firstTagview.marg = 6
        firstTagview.btwTags = 6
        firstTagview.btwLines = 8
        
        secondTagview.tagBorderColor = UIColor.white.cgColor
        secondTagview.tagBorderWidth = 1.0
        secondTagview.tagMainBackColor = UIColor.buttonBackground
        secondTagview.tagSecondBackColor = UIColor.clear
        secondTagview.tagSecondTextColor = UIColor.white
        secondTagview.tagFont = UIFont(name: "Montserrat-Regular", size: 14.0)!
        secondTagview.tagContentEdgeInsets = UIEdgeInsets(top: 10, left: 20, bottom: 10, right: 20)
        secondTagview.tagMaximumWidth = .HTagAutoMaximumWidth
        secondTagview.marg = 6
        secondTagview.btwTags = 6
        secondTagview.btwLines = 8
        
        // secondTagview.tagElevation = 0.5
        
        ThirdTagview.tagBorderColor = UIColor.white.cgColor
        ThirdTagview.tagBorderWidth = 1.0
        ThirdTagview.tagMainBackColor = UIColor.buttonBackground
        ThirdTagview.tagSecondBackColor = UIColor.clear
        ThirdTagview.tagSecondTextColor = UIColor.white
        ThirdTagview.tagFont = UIFont(name: "Montserrat-Regular", size: 14.0)!
        ThirdTagview.tagContentEdgeInsets = UIEdgeInsets(top: 10, left: 20, bottom: 10, right: 20)
        ThirdTagview.tagMaximumWidth = .HTagAutoMaximumWidth
        ThirdTagview.marg = 6
        ThirdTagview.btwTags = 6
        ThirdTagview.btwLines = 8
        
        //  ThirdTagview.tagElevation = 0.5
        
        
        
        btnLunch.backgroundColor = UIColor.buttonBackground
        btnLunch.layer.cornerRadius = 5.0
        btnLunch.layer.masksToBounds = true
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnLunchClicked(_ sender: UIButton) {
        let objVC = ConfirmBookingVC.getViewController() as! ConfirmBookingVC
        self.navigationController?.pushViewController(objVC, animated: true)
    }
    
    // MARK: ==================== FUNCTION ===========================
    func setNavigationButton(){
        
        btnMorerOption = UIBarButtonItem(image: UIImage(named: "imgMore"), style: .done, target: self, action: #selector(btnMoreOptionClicked))
        
        let label = UILabel()
        label.font = .custom(size: 20)
        label.textColor = .white
        label.text = "Creation Station"
        label.textAlignment = .left
        
        //  let titleItem = UIBarButtonItem(customView: label)
        navigationItem.rightBarButtonItems = [btnMorerOption]
        
        navigationItem.titleView = label
    }
    @objc func btnMoreOptionClicked(){
        
    }
    
    
    
    
    // MARK: - HTagViewDataSource
    func numberOfTags(_ tagView: HTagView) -> Int {
        
        if tagView == firstTagview {
            return first.count
        }else if tagView == secondTagview {
            return second.count
        }else {
            return third.count
        }
        
        
    }
    
    func tagView(_ tagView: HTagView, titleOfTagAtIndex index: Int) -> String {
        
        
        if tagView == firstTagview {
            return first[index]
        }else if tagView == secondTagview {
            return second[index]
        }else {
            return third[index]
        }
    }
    
    func tagView(_ tagView: HTagView, tagTypeAtIndex index: Int) -> HTagType {
        return .select
        // return .cancel
    }
    
    func tagView(_ tagView: HTagView, tagWidthAtIndex index: Int) -> CGFloat {
        return .HTagAutoWidth
        // return 150
    }
}
