//
//  ConfirmBookingVC.swift
//  MotherShip
//
//  Created by keval dattani on 19/03/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

class ConfirmBookingVC: UIViewController,CreateStationProtocol,UITableViewDelegate,UITableViewDataSource {
   
    

    var btnMorerOption = UIBarButtonItem()
    
    @IBOutlet weak var btnCheckout: UIButton!
    @IBOutlet weak var btnContinue: UIButton!
    @IBOutlet weak var tblConfirmBooking: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

         setNavigationButton()
        // Do any additional setup after loading the view.
    }
    @IBAction func btnContinueCLicked(_ sender: Any) {
    }
    @IBAction func btnCheckoutClicked(_ sender: Any) {
    }
    @objc func btnMoreOptionClicked(){
       
    }
    // MARK: ==================== FUNCTION ===========================
    func setNavigationButton(){
        
        btnMorerOption = UIBarButtonItem(image: UIImage(named: "imgMore"), style: .done, target: self, action: #selector(btnMoreOptionClicked))
        
        let label = UILabel()
        label.font = .custom(size: 20)
        label.textColor = .white
        label.text = "Creation Station"
        label.textAlignment = .left
        
        //  let titleItem = UIBarButtonItem(customView: label)
        navigationItem.rightBarButtonItems = [btnMorerOption]
        
        navigationItem.titleView = label
        
        
        tblConfirmBooking.dataSource = self
        tblConfirmBooking.delegate = self
        
        btnContinue.backgroundColor = UIColor.buttonBackground
        btnContinue.layer.cornerRadius = 5.0
        btnContinue.layer.masksToBounds = true
        
        btnCheckout.layer.borderWidth = 1.0
        btnCheckout.layer.borderColor = UIColor.white.cgColor
        btnCheckout.layer.cornerRadius = 5.0
        btnCheckout.layer.masksToBounds = true
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblConfirmBooking.dequeueReusableCell(withIdentifier: "ConfirmBookingTC") as! ConfirmBookingTC
        cell.selectionStyle = .none
        return cell
    }
}

