//
//  AlertMothershipVideoVC.swift
//  MotherShip
//
//  Created by Admin on 18/03/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

class AlertMothershipVideoVC: UIViewController, CreateStationProtocol {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var txtEmailPhone: UITextField!
    @IBOutlet weak var btnNext: UIButton!
    
    var intPresent = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnNext.layer.cornerRadius = 5.0
        setUpLayout()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnNextClicked(_ sender: Any) {
        
        dismiss(animated: true, completion: nil)
    }
    
    func setUpLayout(){
        
        if intPresent == 1{
            lblTitle.text = "Mothership Video"
            txtEmailPhone.attributedPlaceholder = NSAttributedString(string:"Enter Mobile number or email address", attributes: [NSAttributedString.Key.foregroundColor: UIColor.placeHolderColor])
        }else if intPresent == 2{
            lblTitle.text = "Skype"
            txtEmailPhone.attributedPlaceholder = NSAttributedString(string:"Enter your skype name", attributes: [NSAttributedString.Key.foregroundColor: UIColor.placeHolderColor])
        }else if intPresent == 3{
            lblTitle.text = "Google Hangouts"
            txtEmailPhone.attributedPlaceholder = NSAttributedString(string:"Enter email address", attributes: [NSAttributedString.Key.foregroundColor: UIColor.placeHolderColor])
        }else{
            
        }
    }
}
