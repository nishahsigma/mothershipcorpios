//
//  AlertIWillComeToYouZipCodeVC.swift
//  MotherShip
//
//  Created by Admin on 18/03/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

class AlertIWillComeToYouZipCodeVC: UIViewController, CreateStationProtocol {
    
    @IBOutlet weak var txtZipCode: UITextField!
    
    @IBOutlet weak var btnPrevious: UIButton!
    @IBOutlet weak var btnNext: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnPrevious.layer.borderColor = UIColor.buttonBackground.cgColor
        btnPrevious.layer.borderWidth = 1
        btnPrevious.layer.cornerRadius = 5.0
        btnNext.layer.cornerRadius = 5.0
    }
    
    @IBAction func btnPreviousClicked(_ sender: Any) {
        dismiss(animated: true) {
            let objVC = AlertMothershipMeetingPlaceVC.getViewController() as! AlertMothershipMeetingPlaceVC
            objVC.modalPresentationStyle = .overCurrentContext
            UIApplication.topViewController()!.present(objVC, animated: true, completion: nil)
        }
    }
    
    @IBAction func btnNextClicked(_ sender: Any) {
        dismiss(animated: true) {
            let objVC = AlertMotherShipStoreCommingSoonVC.getViewController() as! AlertMotherShipStoreCommingSoonVC
            objVC.modalPresentationStyle = .overCurrentContext
            UIApplication.topViewController()!.present(objVC, animated: true, completion: nil)
        }
    }
}
