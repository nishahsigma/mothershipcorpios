//
//  AlertIWillComeToYouSelectStoreVC.swift
//  MotherShip
//
//  Created by Admin on 18/03/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

class AlertIWillComeToYouSelectStoreVC: UIViewController, CreateStationProtocol {
    
    @IBOutlet weak var imgMeetingPlace: UIImageView!
    @IBOutlet weak var imgStore: UIImageView!
    
    @IBOutlet weak var btnMeetingPlace: UIButton!
    @IBOutlet weak var btnStore: UIButton!
    @IBOutlet weak var btnNext: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnNext.layer.cornerRadius = 5.0
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnMeetingPlaceClicked(_ sender: UIButton) {
        isSelect(tag: sender.tag)
    }
    
    @IBAction func btnStoreClicked(_ sender: UIButton) {
        isSelect(tag: sender.tag)
    }
    
    @IBAction func btnNextClicked(_ sender: Any) {
        dismiss(animated: true) {
            if self.imgMeetingPlace.image == UIImage(named: "imgCheck"){
                let objVC = AlertMothershipMeetingPlaceVC.getViewController() as! AlertMothershipMeetingPlaceVC
                objVC.modalPresentationStyle = .overCurrentContext
                 UIApplication.topViewController()!.present(objVC, animated: true, completion: nil)
            }else{
                let objVC = AlertIWillComeToYouZipCodeVC.getViewController() as! AlertIWillComeToYouZipCodeVC
                objVC.modalPresentationStyle = .overCurrentContext
                UIApplication.topViewController()!.present(objVC, animated: true, completion: nil)
            }
        }
    }
    
    @objc func isSelect(tag:Int){
        
        switch tag {
        case 1:
            imgMeetingPlace.image = UIImage(named: "imgCheck")
            imgStore.image = UIImage(named: "imgUncheck")
        case 2:
            imgMeetingPlace.image = UIImage(named: "imgUncheck")
            imgStore.image = UIImage(named: "imgCheck")
        default:
            print("")
        }
    }
}
