//
//  AlertComeToMeVC.swift
//  MotherShip
//
//  Created by Admin on 18/03/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

class AlertComeToMeVC: UIViewController, CreateStationProtocol {
    
    @IBOutlet weak var txtEmailAddress: UITextField!
    @IBOutlet weak var txtApt: UITextField!
    @IBOutlet weak var txtCity: UITextField!
    @IBOutlet weak var txtState: UITextField!
    @IBOutlet weak var txtZipCode: UITextField!
    @IBOutlet weak var txtViewIntrusction: UITextView!
    @IBOutlet weak var btnNext: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnNext.layer.cornerRadius = 5.0
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnNextClicked(_ sender: Any) {
        
        dismiss(animated: true, completion: nil)
    }
}
