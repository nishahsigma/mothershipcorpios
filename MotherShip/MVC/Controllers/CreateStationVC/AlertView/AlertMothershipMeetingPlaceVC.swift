//
//  AlertMothershipMeetingPlaceVC.swift
//  MotherShip
//
//  Created by Admin on 18/03/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

class AlertMothershipMeetingPlaceVC: UIViewController, CreateStationProtocol {
    
    @IBOutlet weak var imgStarbucks: UIImageView!
    @IBOutlet weak var imgLibrary: UIImageView!
    @IBOutlet weak var imgCoworkSpace: UIImageView!
    @IBOutlet weak var imgEmailCustomLocation: UIImageView!
    
    @IBOutlet weak var btnStarbucks: UIButton!
    @IBOutlet weak var btnLibrary: UIButton!
    @IBOutlet weak var btnCoworkSpace: UIButton!
    @IBOutlet weak var btnEmailCusstomLocation: UIButton!
    
    @IBOutlet weak var btnPrevious: UIButton!
    @IBOutlet weak var btnNext: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnPrevious.layer.borderColor = UIColor.buttonBackground.cgColor
        btnPrevious.layer.borderWidth = 1
        btnPrevious.layer.cornerRadius = 5.0
        btnNext.layer.cornerRadius = 5.0
        
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnPreviousClicked(_ sender: Any) {
        
        dismiss(animated: true) {
            let objVC = AlertIWillComeToYouSelectStoreVC.getViewController() as! AlertIWillComeToYouSelectStoreVC
            objVC.modalPresentationStyle = .overCurrentContext
            UIApplication.topViewController()!.present(objVC, animated: true, completion: nil)
        }
    }
    
    @IBAction func btnNextClicked(_ sender: Any) {
       
        dismiss(animated: true) {
            let objVC = AlertIWillComeToYouZipCodeVC.getViewController() as! AlertIWillComeToYouZipCodeVC
            objVC.modalPresentationStyle = .overCurrentContext
            UIApplication.topViewController()!.present(objVC, animated: true, completion: nil)
        }
    }
    
    @IBAction func btnStarbucksClicked(_ sender: UIButton) {
        isSelect(tag: sender.tag)
    }
    
    @IBAction func btnLibraryClicked(_ sender: UIButton) {
        isSelect(tag: sender.tag)
    }
    
    @IBAction func btnCoworkSpaceClicked(_ sender: UIButton) {
        isSelect(tag: sender.tag)
    }
    
    @IBAction func btnEmailCusstomLocationClicked(_ sender: UIButton) {
        isSelect(tag: sender.tag)
    }
    
    @objc func isSelect(tag:Int){
        
        switch tag {
        case 1:
            imgStarbucks.image = UIImage(named: "imgCheck")
            imgLibrary.image = UIImage(named: "imgUncheck")
            imgCoworkSpace.image = UIImage(named: "imgUncheck")
            imgEmailCustomLocation.image = UIImage(named: "imgUncheck")
        case 2:
            imgStarbucks.image = UIImage(named: "imgUncheck")
            imgLibrary.image = UIImage(named: "imgCheck")
            imgCoworkSpace.image = UIImage(named: "imgUncheck")
            imgEmailCustomLocation.image = UIImage(named: "imgUncheck")
        case 3:
            imgStarbucks.image = UIImage(named: "imgUncheck")
            imgLibrary.image = UIImage(named: "imgUncheck")
            imgCoworkSpace.image = UIImage(named: "imgCheck")
            imgEmailCustomLocation.image = UIImage(named: "imgUncheck")
        case 4:
            imgStarbucks.image = UIImage(named: "imgUncheck")
            imgLibrary.image = UIImage(named: "imgUncheck")
            imgCoworkSpace.image = UIImage(named: "imgUncheck")
            imgEmailCustomLocation.image = UIImage(named: "imgCheck")
        default:
            print("")
        }
    }
}
