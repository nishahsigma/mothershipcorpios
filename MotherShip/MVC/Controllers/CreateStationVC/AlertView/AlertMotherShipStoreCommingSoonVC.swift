//
//  AlertMotherShipStoreCommingSoonVC.swift
//  MotherShip
//
//  Created by Admin on 18/03/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

class AlertMotherShipStoreCommingSoonVC: UIViewController, CreateStationProtocol {
    
    
    @IBOutlet weak var imgAgree: UIImageView!
    
    @IBOutlet weak var btnAgree: UIButton!
    @IBOutlet weak var btnPrevious: UIButton!
    @IBOutlet weak var btnSubmit: UIButton!
    
    var isCheck = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnPrevious.layer.borderColor = UIColor.buttonBackground.cgColor
        btnPrevious.layer.borderWidth = 1
        btnPrevious.layer.cornerRadius = 5.0
        btnSubmit.layer.cornerRadius = 5.0
    }
    
    @IBAction func btnAgreeClicked(_ sender: Any) {
        
        if isCheck == 1{
            isCheck = 0
            imgAgree.image = UIImage(named: "imgCheckBox")
        }else{
            isCheck = 1
            imgAgree.image = UIImage(named: "imgCheckBoxSelect")
        }
    }
    
    @IBAction func btnPreviousClicked(_ sender: Any) {
        
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnSubmitClicked(_ sender: Any) {
        
        if isCheck == 1{
            
            self.presentingViewController?.dismiss(animated: true, completion: {
                    let objVC = BookserviceProviderVC.getViewController() as! BookserviceProviderVC
                objVC.modalPresentationStyle = .overCurrentContext
                UIApplication.topViewController()?.navigationController!.pushViewController(objVC, animated: true)
                })
        }else{
            
        }
    }
    
}
