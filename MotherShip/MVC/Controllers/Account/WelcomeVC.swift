//
//  WelcomeVC.swift
//  MotherShip
//
//  Created by Admin on 02/03/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit
import Kingfisher

class WelcomeVC: UIViewController, MainProtocol {
    
    @IBOutlet weak var imgCarousel: ZKCarousel!
    
    @IBOutlet weak var btnSignup: UIButton!
    @IBOutlet weak var btnLogin: UIButton!
    
    @IBOutlet weak var lblAccount: UILabel!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupCarousel()
        
        lblAccount.text = "Already have an account?"
        lblAccount.font = .regular()
        lblAccount.textColor = .lightGrayText
    }
    
    // MARK: ==================== BUTTON CLICK ===========================
    @IBAction func btnSignupClicked(_ sender: Any) {
        goToDashboard()
//        let objVC =  HomeVC.getViewController() as! HomeVC
//        self.navigationController?.pushViewController(objVC, animated: true)
    }
    
    @IBAction func btnLoginClicked(_ sender: Any) {
        
        let objVC =  LoginVC.getViewController() as! LoginVC
        self.navigationController?.pushViewController(objVC, animated: true)
    }
    
    
    // MARK: ==================== SETUP CAROUSEL VIEW ===========================
    private func setupCarousel() {
        
        // Create as many slides as you'd like to show in the carousel
        let slide = ZKCarouselSlide(image: UIImage(named: "1")!, title: "BOOK FOR AN HOUR AND GET QUOTE.", description: "Book and pay for your first discovery hour to receive preliminary advice on your project, how much tome to allocate and scope of work.")
        
        let slide1 = ZKCarouselSlide(image: UIImage(named: "2")!, title: "COLLABORATE ON DESKTOP OR MOBILE OR IN PERSON IF YOU CHOOSE.", description: "Manage projects on trusted Mothership communication tools.Get work done.")
        
        let slide2 = ZKCarouselSlide(image: UIImage(named: "3")!, title: "PAY AFTER MILESTONE ARE COMPLETED AND APPROVED", description: "Pusha button to submit approval and your payment is automatically deployed.")
        
        let slide3 = ZKCarouselSlide(image: UIImage(named: "4")!, title: "ADD EXTRA HOURS,SWITCH OR ADD A\n PROVIDER WITH THE TOUCH OF A BUTTON", description: "No need to vet or interview new candidates.\n Our star providers simply show up.")
        
        let slide4 = ZKCarouselSlide(image: UIImage(named: "5")!, title: "WHAT HAPPENS MOTHERSHIP STAAYS ON MOTHERSHIP.", description: "Providers are bounded by NDA and all details of your projects are centerlly stored and secure.")
        
        let slide5 = ZKCarouselSlide(image: UIImage(named: "6")!, title: "NO NEED TO POST A JOB TO GET A PROPOSAL.\n NO NEED TO INTERVIEW CANDIDATES.", description: "Select the service you need and a few other\n cimple criteria.Your start rated provider will be")
        
        let slide6 = ZKCarouselSlide(image: UIImage(named: "7")!, title: "ACCESS CREATIVE AND PROFESSIOANL SERVICE PROVIDESR.", description: "Book by the service and work with highly\n vetted star rated pro's")
        
        let slide7 = ZKCarouselSlide(image: UIImage(named: "8")!, title: "ACCESS CREATIVE AND PROFESSIOANL SERVICE PROVIDESR.", description: "Book by the service and work with highly\n vetted star rated pro's")
        
        let slide8 = ZKCarouselSlide(image: UIImage(named: "9")!, title: "WANT MORE? WANT NO MORE.ENJOY\n ACCESS TO ANCILIARY PRODUCTS.", description: "Get flex workspace,print,ship,copy,events,\n Streaming content and brain training software.")
        
        
        // Add the slides to the carousel
        self.imgCarousel.slides = [slide, slide1, slide2, slide3, slide4, slide5, slide6, slide7, slide8]
        self.imgCarousel.interval = 2.5
        self.imgCarousel.start()
        
    }
    
    
}

