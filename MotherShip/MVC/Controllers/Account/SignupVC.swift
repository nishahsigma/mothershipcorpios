//
//  SignupVC.swift
//  MotherShip
//
//  Created by Admin on 03/03/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

class SignupVC: UIViewController , MainProtocol{
    
    @IBOutlet weak var txtUsername: SkyFloatingLabelTextField!
    @IBOutlet weak var txtEmail: SkyFloatingLabelTextField!
    @IBOutlet weak var txtPassword: SkyFloatingLabelTextField!
    @IBOutlet weak var txtConfirmPassword: SkyFloatingLabelTextField!
    @IBOutlet weak var txtPhoneNumber: SkyFloatingLabelTextField!
    @IBOutlet weak var txtAddress: SkyFloatingLabelTextField!
    @IBOutlet weak var txtBillingAddress: SkyFloatingLabelTextField!
    @IBOutlet weak var txtSkypeName: SkyFloatingLabelTextField!
    @IBOutlet weak var txtHangoutID: SkyFloatingLabelTextField!
    @IBOutlet weak var txtVideoID: SkyFloatingLabelTextField!
    
    @IBOutlet weak var imgTickEmail: UIImageView!
    @IBOutlet weak var imgTickPassword: UIImageView!
    @IBOutlet weak var imgTickConfirmPassword: UIImageView!
    
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var btnSignup: UIButton!
    @IBOutlet weak var btnShowPassword: UIButton!
    @IBOutlet weak var btnConfirmShowPassword: UIButton!
    
    var isShowPassword = false
    var isShowConfirmPassword = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setLayout()
    }
    
    // MARK: ==================== BUTTON CLICK ===========================
    @IBAction func btnLoginClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSignupClicked(_ sender: Any) {
        
        if  isValid(){
            let objVC =  LoginVC.getViewController() as! LoginVC
            self.navigationController?.pushViewController(objVC, animated: true)
        }
    }
    
    @IBAction func btnShowPasswrodClicked(_ sender: UIButton) {
        
        if isShowPassword{
            isShowPassword = false
            sender.setImage(UIImage(named:"imgEye"), for: .normal)
            self.txtPassword.isSecureTextEntry = true
        }else{
            isShowPassword = true
            sender.setImage(UIImage(named:"imgEyeHide"), for: .normal)
            self.txtPassword.isSecureTextEntry = false
        }
    }
    
    @IBAction func btnShowConfirmPasswrodClicked(_ sender: UIButton) {
        
        if isShowConfirmPassword{
            isShowConfirmPassword = false
            sender.setImage(UIImage(named:"imgEye"), for: .normal)
            self.txtConfirmPassword.isSecureTextEntry = true
        }else{
            isShowConfirmPassword = true
            sender.setImage(UIImage(named:"imgEyeHide"), for: .normal)
            self.txtConfirmPassword.isSecureTextEntry = false
        }
    }
    
    // MARK: ==================== FUNCTION ===========================
    
    func setLayout(){
        btnSignup.layer.borderColor = UIColor.white.cgColor
        btnSignup.layer.borderWidth = 1
        
        imgTickEmail.alpha = 0.0
        
        imgTickPassword.alpha = 0.0
        imgTickPassword.isHidden = true
        
        imgTickConfirmPassword.alpha = 0.0
        imgTickConfirmPassword.isHidden = true
        
        btnShowPassword.imageView?.contentMode = .scaleAspectFit
        btnConfirmShowPassword.imageView?.contentMode = .scaleAspectFit
        
        btnLogin.layer.cornerRadius = 5
        btnSignup.layer.cornerRadius = 5
    }
    
    func isValid() -> Bool{
        if txtUsername.text!.isEmpty{
            Helper.globalAlert(msg: "Please enter username")
            txtUsername.resignFirstResponder()
            return false
        }else if txtEmail.text!.isEmpty{
            Helper.globalAlert(msg: "Please enter email")
            txtEmail.resignFirstResponder()
            return false
        }else if txtPassword.text!.isEmpty{
            Helper.globalAlert(msg: "Please enter password")
            txtPassword.resignFirstResponder()
            return false
        }else if txtConfirmPassword.text!.isEmpty{
            Helper.globalAlert(msg: "Please enter confirm password")
            txtConfirmPassword.resignFirstResponder()
            return false
        }else if txtPhoneNumber.text!.isEmpty{
            Helper.globalAlert(msg: "Please enter phone number")
            txtPhoneNumber.resignFirstResponder()
            return false
        }else if txtPassword.text! != txtConfirmPassword.text{
            Helper.globalAlert(msg: "Password and confirm password are not matched")
            txtPhoneNumber.resignFirstResponder()
            return false
        }else{
            return true
        }
    }
    
    // MARK: ==================== WEBSERVICE CALLING ===========================
    
    
    
}

// MARK: ==================== EXTENSION ===========================
extension SignupVC: UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == txtEmail{
            
            if let text = textField.text as NSString? {
                let strUpdated = text.replacingCharacters(in: range, with: string)
                print(strUpdated)
                
                let isEmailValid = Helper.isValidEmail(strUpdated)
                if isEmailValid{
                  self.imgTickEmail.alpha = 1.0
                 }else{
                     self.imgTickEmail.alpha = 0.0
                 }
            }
            return true
            
        }else if textField == txtPassword{
            
            if let text = textField.text as NSString? {
                let strUpdated = text.replacingCharacters(in: range, with: string)
                print(strUpdated)
                
                if strUpdated.count >= 6{
                     self.imgTickPassword.isHidden = false
                    self.imgTickPassword.alpha = 1.0
                 }else{
                    self.imgTickPassword.alpha = 0.0
                    self.imgTickPassword.isHidden = true
                 }
            }
            return true
        }else if textField == txtConfirmPassword{
            
            if let text = textField.text as NSString? {
                let strUpdated = text.replacingCharacters(in: range, with: string)
                print(strUpdated)
                
                if strUpdated.count >= 6{
                    self.imgTickConfirmPassword.isHidden = false
                    self.imgTickConfirmPassword.alpha = 1.0
                 }else{
                    self.imgTickConfirmPassword.alpha = 0.0
                    self.imgTickConfirmPassword.isHidden = true
                    
                }
            }
            return true
        }else{
            return true
        }
    }
}
