//
//  LoginVC.swift
//  MotherShip
//
//  Created by Admin on 03/03/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

class LoginVC: UIViewController , MainProtocol{
    
    @IBOutlet weak var txtEmail: SkyFloatingLabelTextField!
    @IBOutlet weak var txtPassword: SkyFloatingLabelTextField!
    
    @IBOutlet weak var imgTickEmail: UIImageView!
    @IBOutlet weak var imgTickPassword: UIImageView!
    
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var btnSignup: UIButton!
    @IBOutlet weak var btnShowPassword: UIButton!
    
    var isShowPassword = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setLayout()
    }
    
    // MARK: ==================== BUTTON CLICK ===========================
    @IBAction func btnLoginClicked(_ sender: Any) {
        
        if isValid(){
            goToDashboard()
        }
    }
    
    @IBAction func btnSignupClicked(_ sender: Any) {
        
        let objVC =  SignupVC.getViewController() as! SignupVC
        self.navigationController?.pushViewController(objVC, animated: true)
    }
    
    @IBAction func btnShowPasswrodClicked(_ sender: UIButton) {
        
        if isShowPassword{
            isShowPassword = false
            sender.setImage(UIImage(named:"imgEye"), for: .normal)
            self.txtPassword.isSecureTextEntry = true
        }else{
            isShowPassword = true
            sender.setImage(UIImage(named:"imgEyeHide"), for: .normal)
            self.txtPassword.isSecureTextEntry = false
        }
    }
    
    // MARK: ==================== FUNCTION ===========================
    
    func setLayout(){
        btnSignup.layer.borderColor = UIColor.white.cgColor
        btnSignup.layer.borderWidth = 1
        
        imgTickEmail.alpha = 0.0
        imgTickPassword.alpha = 0.0
        imgTickPassword.isHidden = true
        
        btnShowPassword.imageView?.contentMode = .scaleAspectFit
        
        btnLogin.layer.cornerRadius = 5
        btnSignup.layer.cornerRadius = 5
    }
    
    func isValid() -> Bool{
        if txtEmail.text!.isEmpty{
            Helper.globalAlert(msg: "Please Enter Email")
            txtEmail.resignFirstResponder()
            return false
        }else if txtPassword.text!.isEmpty{
            Helper.globalAlert(msg: "Please Enter Password")
            txtPassword.resignFirstResponder()
            return false
        }else{
            return true
        }
    }
    
    
    
    
    // MARK: ==================== WEBSERVICE CALLING ===========================
    
    
    
    
}

// MARK: ==================== EXTENSION ===========================
extension LoginVC: UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == txtEmail{
            
            if let text = textField.text as NSString? {
                let strUpdated = text.replacingCharacters(in: range, with: string)
                print(strUpdated)
                
                let isEmailValid = Helper.isValidEmail(strUpdated)
                if isEmailValid{
                    UIView.animate(withDuration: 0.5) {
                        self.imgTickEmail.alpha = 1.0
                    }
                }else{
                    UIView.animate(withDuration: 0.5) {
                        self.imgTickEmail.alpha = 0.0
                    }
                }
            }
            
            return true
        }else if textField == txtPassword{
            
            if let text = textField.text as NSString? {
                let strUpdated = text.replacingCharacters(in: range, with: string)
                print(strUpdated)
                
                if strUpdated.count >= 6{
                    self.imgTickPassword.isHidden = false
                    self.imgTickPassword.alpha = 1.0
                }else{
                    self.imgTickPassword.alpha = 0.0
                    self.imgTickPassword.isHidden = true
                }
            }
            return true
        }else{
            return true
        }
    }
}
